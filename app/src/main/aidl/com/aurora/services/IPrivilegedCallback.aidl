package com.aurora.services;

interface IPrivilegedCallback {

    void handleResult(in String packageName, in int returnCode);

}