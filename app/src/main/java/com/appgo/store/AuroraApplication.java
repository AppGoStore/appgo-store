package com.appgo.store;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;

import androidx.room.Room;

import com.appgo.store.db.AppDatabase;
import com.appgo.store.events.Event;
import com.appgo.store.events.RxBus;
import com.appgo.store.installer.Installer;
import com.appgo.store.installer.InstallerService;
import com.appgo.store.model.App;
import com.appgo.store.util.Log;
import com.appgo.store.util.Util;
import com.dragons.aurora.playstoreapiv2.GooglePlayAPI;
import com.facebook.stetho.Stetho;
import com.huawei.hms.analytics.HiAnalytics;
import com.huawei.hms.analytics.HiAnalyticsInstance;
import com.huawei.hms.analytics.HiAnalyticsTools;
import com.jakewharton.rxrelay2.Relay;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import io.reactivex.plugins.RxJavaPlugins;

public class AuroraApplication extends Application {

    public static final String DATABASE = "database";
    private static AppDatabase database = null;
    private static HiAnalyticsInstance analyticsInstance = null;

    public static GooglePlayAPI api = null;
    private static RxBus rxBus = null;
    private static boolean bulkUpdateAlive = false;
    private static List<App> ongoingUpdateList = new ArrayList<>();

    @SuppressLint("StaticFieldLeak")
    private static Installer installer;

    private BroadcastReceiver packageUninstallReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getData() != null) {
                final String packageName = intent.getData().getSchemeSpecificPart();
                if (packageName != null)
                    rxNotify(new Event(Event.SubType.UNINSTALLED, packageName));
            }
        }
    };

    public static RxBus getRxBus() {
        return rxBus;
    }

    public static Relay<Event> getRelayBus() {
        return rxBus.getBus();
    }

    public static boolean isBulkUpdateAlive() {
        return bulkUpdateAlive;
    }

    public static void setBulkUpdateAlive(boolean updating) {
        AuroraApplication.bulkUpdateAlive = updating;
    }

    public static List<App> getOngoingUpdateList() {
        return ongoingUpdateList;
    }

    public static void setOngoingUpdateList(List<App> ongoingUpdateList) {
        AuroraApplication.ongoingUpdateList = ongoingUpdateList;
    }

    public static void removeFromOngoingUpdateList(String packageName) {
        Iterator<App> iterator = ongoingUpdateList.iterator();
        while (iterator.hasNext()) {
            if (packageName.equals(iterator.next().getPackageName()))
                iterator.remove();
        }
        if (ongoingUpdateList.isEmpty())
            setBulkUpdateAlive(false);
    }

    public static Installer getInstaller() {
        return installer;
    }

    public static void rxNotify(Event event) {
        rxBus.getBus().accept(event);
    }

    public static AppDatabase getDatabase() {
        return database;
    }

    public static HiAnalyticsInstance getHiAnalytics() {
        return analyticsInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        // Enable Analytics Kit Log
        HiAnalyticsTools.enableLog();
        // Generate the Analytics Instance
        analyticsInstance = HiAnalytics.getInstance(this);

        //Clear preferences for app version below 3.2.6
        if (BuildConfig.VERSION_CODE < 26) {
            SharedPreferences preferences = Util.getPrefs(this);
            preferences.edit().clear().apply();
        }

        rxBus = new RxBus();
        installer = new Installer(this);
        database = Room.databaseBuilder(this, AppDatabase.class, DATABASE).build();

        //Clear all old installation sessions.
        Util.clearOldInstallationSessions(this);

        //Check & start notification service
        Util.startNotificationService(this);

        //Register global install/uninstall broadcast receiver.
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addDataScheme("package");
        intentFilter.addAction(Intent.ACTION_PACKAGE_REMOVED);
        intentFilter.addAction(Intent.ACTION_PACKAGE_FULLY_REMOVED);
        intentFilter.addAction(Intent.ACTION_UNINSTALL_PACKAGE);
        registerReceiver(packageUninstallReceiver, intentFilter);

        registerReceiver(installer.getPackageInstaller().getBroadcastReceiver(),
                new IntentFilter(InstallerService.ACTION_INSTALLATION_STATUS_NOTIFICATION));

        //Global RX-Error handler, just simply logs, I make sure all errors are handled at origin.
        RxJavaPlugins.setErrorHandler(throwable -> {
            Log.e(throwable.getMessage());
            if (BuildConfig.DEBUG) {
                throwable.printStackTrace();
            }
        });

        if (BuildConfig.DEBUG) {
            Stetho.initializeWithDefaults(this);
        }
    }

    @Override
    public void onTerminate() {
        try {
            unregisterReceiver(packageUninstallReceiver);
            unregisterReceiver(installer.getPackageInstaller().getBroadcastReceiver());
        } catch (Exception ignored) {
        }
        super.onTerminate();
    }
}
