package com.appgo.store;

import android.content.Context;

import com.appgo.store.util.PrefUtil;
import com.appgo.store.util.Util;

import java.util.ArrayList;
import java.util.List;

public class TokenDispenserMirrors {

    private static List<String> dispenserList = new ArrayList<>();

    static {
        dispenserList.add("http://auroraoss.com:8080");
        dispenserList.add("http://auroraoss.in:8080");
        //dispenserList.add("http://92.42.46.11:8080");
        //dispenserList.add("https://token-dispenser.calyxinstitute.org");
    }

    public static String get(Context context) {
        if (Util.isCustomTokenizerEnabled(context))
            return Util.getCustomTokenizerURL(context);
        else
            return Util.getTokenizerURL(context);
    }

    public static void setNextDispenser(Context context, int dispenserNum) {
        PrefUtil.putString(context, Constants.PREFERENCE_TOKENIZER_URL, dispenserList.get(dispenserNum % dispenserList.size()));
    }
}
