package com.appgo.store.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.appgo.store.R;
import com.appgo.store.model.ExodusTracker;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ExodusAdapter extends RecyclerView.Adapter<ExodusAdapter.ViewHolder> {

    private Context context;
    private List<ExodusTracker> exodusTrackers;

    public ExodusAdapter(Context context, List<ExodusTracker> exodusTrackers) {
        this.context = context;
        this.exodusTrackers = exodusTrackers;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_exodus, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ExodusTracker mExodusTracker = exodusTrackers.get(position);
        holder.TrackerName.setText(mExodusTracker.Name);
        holder.TrackerSignature.setText(mExodusTracker.Signature);
        holder.TrackerDate.setText(mExodusTracker.Date);
        holder.itemView.setOnClickListener(v -> context.startActivity(new Intent(Intent.ACTION_VIEW,
                Uri.parse(mExodusTracker.URL)))
        );
    }

    @Override
    public int getItemCount() {
        return exodusTrackers.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tracker_name)
        TextView TrackerName;
        @BindView(R.id.tracker_signature)
        TextView TrackerSignature;
        @BindView(R.id.tracker_date)
        TextView TrackerDate;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
