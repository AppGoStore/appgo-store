package com.appgo.store.db;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.appgo.store.model.AppModel;

import java.util.List;

@Dao
public interface AppDao {

	@Query("SELECT * FROM AppModel")
	List<AppModel> getAll();

	@Query("SELECT * FROM AppModel WHERE top ")
	List<AppModel> getTop();

	@Query("SELECT * FROM AppModel WHERE packageName = :packageName")
	AppModel getApp(String packageName);

	@Query("SELECT * FROM AppModel WHERE category = :category")
	List<AppModel> getAppByCategory(String category);

	@Insert
	void insert(AppModel entity);

	@Update
	void update(AppModel entity);

	@Delete
	void delete(AppModel entity);

	@Query("DELETE FROM AppModel")
	void truncate();
}
