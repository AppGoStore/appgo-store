package com.appgo.store.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.appgo.store.model.AppCategoryModel;
import com.appgo.store.model.AppModel;

@Database(entities = {AppModel.class, AppCategoryModel.class}, version = 2)
public abstract class AppDatabase extends RoomDatabase {
    public abstract AppDao applications();

    public abstract CategoryDao categories();
}