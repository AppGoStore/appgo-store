package com.appgo.store.db;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.appgo.store.model.AppCategoryModel;

import java.util.List;

@Dao
public interface CategoryDao {

	@Query("SELECT * FROM AppCategoryModel")
	List<AppCategoryModel> getAll();

	@Insert
	void insert(AppCategoryModel entity);

	@Update
	void update(AppCategoryModel entity);

	@Delete
	void delete(AppCategoryModel entity);

	@Query("DELETE FROM AppCategoryModel")
	void truncate();
}
