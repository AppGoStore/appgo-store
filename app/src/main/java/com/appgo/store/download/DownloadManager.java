package com.appgo.store.download;

import android.content.Context;

import com.appgo.store.BuildConfig;
import com.appgo.store.Constants;
import com.appgo.store.util.NetworkInterceptor;
import com.appgo.store.util.Util;
import com.tonyodev.fetch2.Download;
import com.tonyodev.fetch2.Fetch;
import com.tonyodev.fetch2.FetchConfiguration;
import com.tonyodev.fetch2.FetchListener;
import com.tonyodev.fetch2okhttp.OkHttpDownloader;

import java.util.List;

import okhttp3.OkHttpClient;

public class DownloadManager {

    private static volatile DownloadManager instance;
    private static Fetch fetch;

    public DownloadManager() {
        if (instance != null) {
            throw new RuntimeException("Use get() method to get the single instance of RxBus");
        }
    }

    public static Fetch getFetchInstance(Context context) {
        if (instance == null) {
            synchronized (DownloadManager.class) {
                if (instance == null) {
                    instance = new DownloadManager();
                    fetch = getFetch(context);
                }
            }
        }
        return fetch;
    }

    private static Fetch getFetch(Context context) {
        FetchConfiguration.Builder fetchConfiguration = new FetchConfiguration.Builder(context)
                .setDownloadConcurrentLimit(Util.getActiveDownloadCount(context))
                .setHttpDownloader(new OkHttpDownloader(getOkHttpClient(context), Util.getDownloadStrategy(context)))
                .setNamespace(Constants.TAG)
                .enableLogging(Util.isFetchDebugEnabled(context))
                .enableHashCheck(true)
                .enableFileExistChecks(true)
                .enableRetryOnNetworkGain(true)
                .enableAutoStart(true)
                .setAutoRetryMaxAttempts(3)
                .setProgressReportingInterval(3000);
        return Fetch.Impl.getInstance(fetchConfiguration.build());
    }

    private static OkHttpClient getOkHttpClient(Context context) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        if (Util.isNetworkProxyEnabled(context))
            builder.proxy(Util.getNetworkProxy(context));
        if (BuildConfig.DEBUG) {
            builder.addNetworkInterceptor(new NetworkInterceptor());
        }
        return builder.build();
    }

    public static void updateOngoingDownloads(Fetch fetch, List<String> packageList, Download download,
                                              FetchListener fetchListener) {
        if (packageList.contains(download.getTag())) {
            final String packageName = download.getTag();
            if (packageName != null) {
                fetch.deleteGroup(packageName.hashCode());
                packageList.remove(packageName);
            }
        }
        if (packageList.size() == 0) {
            fetch.removeListener(fetchListener);
        }
    }
}

