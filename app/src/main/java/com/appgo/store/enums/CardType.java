package com.appgo.store.enums;

public enum CardType {
    LEGACY,
    MODERN
}
