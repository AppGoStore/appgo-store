package com.appgo.store.enums;

public enum ErrorType {
    NO_NETWORK,
    NO_DOWNLOADS,
    NO_API,
    UNKNOWN,
    SESSION_EXPIRED,
    APP_NOT_FOUND,
    LOGOUT_ERR
}
