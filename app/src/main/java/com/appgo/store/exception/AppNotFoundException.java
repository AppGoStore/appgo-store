package com.appgo.store.exception;

import java.io.IOException;

public class AppNotFoundException extends IOException {

    protected int code;

    public AppNotFoundException() {
        super("AppNotFoundException");
    }

    public AppNotFoundException(String message, int code) {
        super(message);
        this.code = code;
    }

    public AppNotFoundException(String message) {
        super(message);
    }

    public AppNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
