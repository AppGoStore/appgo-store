package com.appgo.store.exception;

import com.dragons.aurora.playstoreapiv2.AuthException;

public class InvalidApiException extends AuthException {

    protected int code;

    public InvalidApiException() {
        super("InvalidApiException");
    }

    public InvalidApiException(String message, int code) {
        super(message);
        this.code = code;
    }

    public InvalidApiException(String message) {
        super(message);
    }

    public InvalidApiException(String message, Throwable cause) {
        super(message, cause);
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
