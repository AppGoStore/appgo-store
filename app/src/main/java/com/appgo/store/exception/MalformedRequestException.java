package com.appgo.store.exception;

import java.io.IOException;

public class MalformedRequestException extends IOException {

    protected int code;

    public MalformedRequestException() {
        super("MalformedRequestException");
    }

    public MalformedRequestException(String message, int code) {
        super(message);
        this.code = code;
    }

    public MalformedRequestException(String message) {
        super(message);
    }

    public MalformedRequestException(String message, Throwable cause) {
        super(message, cause);
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
