package com.appgo.store.exception;

import java.io.IOException;

public class NotPurchasedException extends IOException {

    protected int code;

    public NotPurchasedException(String message, int code) {
        super(message);
        this.code = code;
    }

    public NotPurchasedException() {
        super("NotPurchasedException");
    }

    public NotPurchasedException(String message) {
        super(message);
    }

    public NotPurchasedException(String message, Throwable cause) {
        super(message, cause);
    }
}
