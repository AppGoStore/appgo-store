package com.appgo.store.exception;

import java.io.IOException;

public class TokenizerException extends IOException {

    public TokenizerException() {
        super("TokenizerException");
    }

    public TokenizerException(String message) {
        super(message);
    }

    public TokenizerException(String message, Throwable cause) {
        super(message, cause);
    }
}
