package com.appgo.store.exception;

import java.io.IOException;

public class TooManyRequestsException extends IOException {

    protected int code;

    public TooManyRequestsException() {
        super("TooManyRequestsException");
    }

    public TooManyRequestsException(String message, int code) {
        super(message);
        this.code = code;
    }

    public TooManyRequestsException(String message) {
        super(message);
    }

    public TooManyRequestsException(String message, Throwable cause) {
        super(message, cause);
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
