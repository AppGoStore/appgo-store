package com.appgo.store.exception;

import com.dragons.aurora.playstoreapiv2.AuthException;

public class TwoFactorAuthException extends AuthException {

    protected int code;

    public TwoFactorAuthException() {
        super("");
    }

    public TwoFactorAuthException(String message, int code) {
        super(message);
        this.code = code;
    }

    public TwoFactorAuthException(String message) {
        super(message);
    }

    public TwoFactorAuthException(String message, Throwable cause) {
        super(message, cause);
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
