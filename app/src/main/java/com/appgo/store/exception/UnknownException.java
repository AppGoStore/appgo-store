package com.appgo.store.exception;

import java.io.IOException;

public class UnknownException extends IOException {

    protected int code;

    public UnknownException() {
        super("UnknownException");
    }

    public UnknownException(String message, int code) {
        super(message);
        this.code = code;
    }

    public UnknownException(String message) {
        super(message);
    }

    public UnknownException(String message, Throwable cause) {
        super(message, cause);
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
