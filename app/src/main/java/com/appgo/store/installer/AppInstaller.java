package com.appgo.store.installer;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInstaller;

import com.appgo.store.util.Log;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

public class AppInstaller extends AppInstallerAbstract {

    private static AppInstaller instance;

    public AppInstaller(Context context) {
        super(context);
        instance = this;
    }

    public static AppInstaller getInstance(Context context) {
        if (instance == null) {
            synchronized (AppInstaller.class) {
                if (instance == null)
                    instance = new AppInstaller(context);
            }
        }
        return instance;
    }

    @Override
    protected void installApkFiles(String packageName, List<File> apkFiles) {
        final PackageInstaller packageInstaller = getContext().getPackageManager().getPackageInstaller();
        try {
            final PackageInstaller.SessionParams sessionParams = new PackageInstaller.SessionParams(PackageInstaller.SessionParams.MODE_FULL_INSTALL);
            final int sessionID = packageInstaller.createSession(sessionParams);
            final PackageInstaller.Session session = packageInstaller.openSession(sessionID);
            for (File apkFile : apkFiles) {
                final InputStream inputStream = new FileInputStream(apkFile);
                final OutputStream outputStream = session.openWrite(apkFile.getName(), 0, apkFile.length());
                IOUtils.copy(inputStream, outputStream);
                session.fsync(outputStream);
                inputStream.close();
                outputStream.close();
            }

            final Intent callbackIntent = new Intent(getContext(), InstallerService.class);
            final PendingIntent pendingIntent = PendingIntent.getService(
                    getContext(),
                    sessionID,
                    callbackIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            session.commit(pendingIntent.getIntentSender());
            session.close();
        } catch (Exception e) {
            Log.e(e.getMessage());
            dispatchSessionUpdate(PackageInstaller.STATUS_FAILURE, packageName);
        }
    }
}
