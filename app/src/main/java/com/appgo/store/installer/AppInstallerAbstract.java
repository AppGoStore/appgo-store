package com.appgo.store.installer;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInstaller;
import android.os.Handler;
import android.os.Looper;

import androidx.annotation.Nullable;

import java.io.File;
import java.util.List;

import lombok.Getter;

@Getter
public abstract class AppInstallerAbstract {

    private Context context;
    private BroadcastReceiver broadcastReceiver;
    private Handler handler = new Handler(Looper.getMainLooper());
    private InstallationStatusListener listener;

    AppInstallerAbstract(Context context) {
        this.context = context.getApplicationContext();
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                int status = intent.getIntExtra(PackageInstaller.EXTRA_STATUS, -1);
                String packageName = intent.getStringExtra(PackageInstaller.EXTRA_PACKAGE_NAME);
                dispatchSessionUpdate(status, packageName);
            }
        };
    }

    void addInstallationStatusListener(InstallationStatusListener listener) {
        this.listener = listener;
    }

    void dispatchSessionUpdate(int status, String packageName) {
        handler.post(() -> {
            if (listener != null)
                listener.onStatusChanged(status, packageName);
        });
    }

    protected abstract void installApkFiles(String packageName, List<File> apkFiles);

    public interface InstallationStatusListener {
        void onStatusChanged(int status, @Nullable String packageName);
    }
}
