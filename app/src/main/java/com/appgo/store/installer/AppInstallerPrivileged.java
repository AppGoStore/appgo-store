package com.appgo.store.installer;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.IBinder;
import android.os.RemoteException;

import com.aurora.services.IPrivilegedCallback;
import com.aurora.services.IPrivilegedService;
import com.appgo.store.BuildConfig;
import com.appgo.store.Constants;
import com.appgo.store.R;
import com.appgo.store.util.ContextUtil;
import com.appgo.store.util.Log;
import com.appgo.store.util.PackageUtil;
import com.appgo.store.util.ViewUtil;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class AppInstallerPrivileged extends AppInstallerAbstract {

    private static final int ACTION_INSTALL_REPLACE_EXISTING = 2;

    private static volatile AppInstallerPrivileged instance;

    private AppInstallerPrivileged(Context context) {
        super(context);
        instance = this;
    }

    public static AppInstallerPrivileged getInstance(Context context) {
        if (instance == null) {
            synchronized (AppInstallerPrivileged.class) {
                if (instance == null)
                    instance = new AppInstallerPrivileged(context);
            }
        }
        return instance;
    }

    private static boolean isServiceOnline(Context context) {
        if (!PackageUtil.isInstalled(context, Constants.SERVICE_PACKAGE)) {
            return false;
        }

        final ServiceConnection serviceConnection = new ServiceConnection() {
            public void onServiceConnected(ComponentName name, IBinder service) {
                Log.i("AppGo Store Service is installed & available");
            }

            public void onServiceDisconnected(ComponentName name) {
                Log.e("AppGo Store Service is installed but unavailable");
            }
        };

        final Intent serviceIntent = new Intent(Constants.PRIVILEGED_EXTENSION_SERVICE_INTENT);
        serviceIntent.setPackage(Constants.PRIVILEGED_EXTENSION_PACKAGE_NAME);

        try {
            context.getApplicationContext().bindService(serviceIntent, serviceConnection, Context.BIND_AUTO_CREATE);
            return true;
        } catch (SecurityException e) {
            return false;
        }
    }

    @Override
    protected void installApkFiles(String packageName, List<File> apkFiles) {
        if (!isServiceOnline(getContext())) {
            ContextUtil.runOnUiThread(this::showDisconnectedServicesDialog);
            return;
        }

        List<Uri> uriList = new ArrayList<>();
        for (File file : apkFiles) {
            uriList.add(Uri.parse(file.getPath()));
        }

        final ServiceConnection serviceConnection = new ServiceConnection() {
            public void onServiceConnected(ComponentName name, IBinder binder) {
                final IPrivilegedService service = IPrivilegedService.Stub.asInterface(binder);
                final IPrivilegedCallback callback = new IPrivilegedCallback.Stub() {
                    @Override
                    public void handleResult(String packageName, int returnCode) {
                        dispatchSessionUpdate(returnCode, packageName);
                    }
                };

                try {
                    service.installSplitPackage(
                            uriList,
                            ACTION_INSTALL_REPLACE_EXISTING,
                            BuildConfig.APPLICATION_ID,
                            callback
                    );
                } catch (RemoteException e) {
                    Log.e("Failed to establish connection to AppGo Store Services");
                }
            }

            public void onServiceDisconnected(ComponentName name) {
                Log.e("Disconnected from AppGo Store Services");
            }
        };

        final Intent serviceIntent = new Intent(Constants.PRIVILEGED_EXTENSION_SERVICE_INTENT);
        serviceIntent.setPackage(Constants.PRIVILEGED_EXTENSION_PACKAGE_NAME);
        getContext().getApplicationContext().bindService(serviceIntent, serviceConnection, Context.BIND_AUTO_CREATE);
    }

    private void showDisconnectedServicesDialog() {
        final MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(getContext());
        builder.setTitle(R.string.action_installations);
        builder.setMessage(R.string.pref_install_mode_offline_services);
        builder.setPositiveButton(android.R.string.ok, (dialog, which) -> dialog.dismiss());

        final int backGroundColor = ViewUtil.getStyledAttribute(getContext(), android.R.attr.colorBackground);
        builder.setBackground(new ColorDrawable(backGroundColor));
        builder.create();
        builder.show();
    }
}