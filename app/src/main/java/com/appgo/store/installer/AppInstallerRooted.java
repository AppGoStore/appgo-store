package com.appgo.store.installer;

import android.content.Context;
import android.content.pm.PackageInstaller;

import com.appgo.store.util.ContextUtil;
import com.appgo.store.util.Log;
import com.appgo.store.util.Root;
import com.appgo.store.util.Util;

import java.io.File;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AppInstallerRooted extends AppInstallerAbstract {

    private static volatile AppInstallerRooted instance;
    private static Root root;

    private AppInstallerRooted(Context context) {
        super(context);
        instance = this;
    }

    public static AppInstallerRooted getInstance(Context context) {
        if (instance == null) {
            synchronized (AppInstallerRooted.class) {
                if (instance == null) {
                    instance = new AppInstallerRooted(context);
                    root = new Root();
                }
            }
        }
        return instance;
    }

    @Override
    protected void installApkFiles(String packageName, List<File> apkFiles) {
        try {
            if (root.isTerminated() || !root.isAcquired()) {
                Root.requestRoot();
                if (!root.isAcquired()) {
                    ContextUtil.toastLong(getContext(), "Root access not available");
                    dispatchSessionUpdate(PackageInstaller.STATUS_FAILURE, packageName);
                    return;
                }
            }

            int totalSize = 0;
            for (File apkFile : apkFiles)
                totalSize += apkFile.length();

            final String createSessionResult = ensureCommandSucceeded(root.exec(String.format(Locale.getDefault(),
                    "pm install-create -i com.android.vending --user %s -r -S %d",
                    Util.getInstallationProfile(getContext()),
                    totalSize)));

            final Pattern sessionIdPattern = Pattern.compile("(\\d+)");
            final Matcher sessionIdMatcher = sessionIdPattern.matcher(createSessionResult);
            boolean found = sessionIdMatcher.find();
            int sessionId = Integer.parseInt(sessionIdMatcher.group(1));

            for (File apkFile : apkFiles)
                ensureCommandSucceeded(root.exec(String.format(Locale.getDefault(),
                        "cat \"%s\" | pm install-write -S %d %d \"%s\"",
                        apkFile.getAbsolutePath(),
                        apkFile.length(),
                        sessionId,
                        apkFile.getName())));

            final String commitSessionResult = ensureCommandSucceeded(root.exec(String.format(Locale.getDefault(),
                    "pm install-commit %d ",
                    sessionId)));

            if (commitSessionResult.toLowerCase().contains("success"))
                dispatchSessionUpdate(PackageInstaller.STATUS_SUCCESS, packageName);
            else
                dispatchSessionUpdate(PackageInstaller.STATUS_FAILURE, packageName);
        } catch (Exception e) {
            Log.w(e.getMessage());
            dispatchSessionUpdate(PackageInstaller.STATUS_FAILURE, packageName);
        }
    }

    private String ensureCommandSucceeded(String result) {
        if (result == null || result.length() == 0)
            throw new RuntimeException(root.readError());
        return result;
    }
}
