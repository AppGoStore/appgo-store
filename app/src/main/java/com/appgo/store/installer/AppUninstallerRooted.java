package com.appgo.store.installer;

import com.appgo.store.model.App;
import com.appgo.store.util.Log;
import com.appgo.store.util.Root;

public class AppUninstallerRooted {

    private Root root;

    public AppUninstallerRooted() {
        root = new Root();
    }

    protected void uninstall(App app) {
        try {
            if (root.isTerminated() || !root.isAcquired()) {
                root = new Root();
                if (!root.isAcquired()) {
                    return;
                }
            }
            Log.d(ensureCommandSucceeded(root.exec("pm clear " + app.getPackageName())));
            Log.d(ensureCommandSucceeded(root.exec("pm uninstall " + app.getPackageName())));
        } catch (Exception e) {
            Log.w(e.getMessage());
        }
    }

    private String ensureCommandSucceeded(String result) {
        if (result == null || result.length() == 0)
            throw new RuntimeException(root.readError());
        return result;
    }
}
