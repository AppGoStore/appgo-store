package com.appgo.store.installer;

import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageInstaller;
import android.os.IBinder;

import androidx.annotation.Nullable;

public class InstallerService extends Service {

    public static final String ACTION_INSTALLATION_STATUS_NOTIFICATION = "com.appgo.store.action.INSTALLATION_STATUS_NOTIFICATION";

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        int status = intent.getIntExtra(PackageInstaller.EXTRA_STATUS, -1);
        String packageName = intent.getStringExtra(PackageInstaller.EXTRA_PACKAGE_NAME);

        //Send broadcast for the installation status of the package
        sendStatusBroadcast(status, packageName);

        //Launch user confirmation activity
        if (status == PackageInstaller.STATUS_PENDING_USER_ACTION) {
            Intent confirmationIntent = intent.getParcelableExtra(Intent.EXTRA_INTENT);
            confirmationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            try {
                startActivity(confirmationIntent);
            } catch (Exception e) {
                sendStatusBroadcast(PackageInstaller.STATUS_FAILURE, packageName);
            }
        }
        stopSelf();
        return START_NOT_STICKY;
    }

    private void sendStatusBroadcast(int status, String packageName) {
        Intent statusIntent = new Intent(ACTION_INSTALLATION_STATUS_NOTIFICATION);
        statusIntent.putExtra(PackageInstaller.EXTRA_STATUS, status);
        statusIntent.putExtra(PackageInstaller.EXTRA_PACKAGE_NAME, packageName);
        sendBroadcast(statusIntent);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
