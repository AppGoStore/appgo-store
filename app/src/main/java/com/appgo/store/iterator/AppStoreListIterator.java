package com.appgo.store.iterator;

import com.appgo.store.model.App;
import com.appgo.store.model.AppBuilder;
import com.appgo.store.model.AppCategoryModel;
import com.appgo.store.model.AppModel;
import com.appgo.store.model.FilterModel;
import com.appgo.store.task.IAppStore;
import com.appgo.store.util.Log;

import java.util.ArrayList;
import java.util.List;

public class AppStoreListIterator implements BaseAppIterator {

    private boolean firstQuery = true;
    private boolean isTop;
    private IAppStore appStore;
    private boolean filterEnabled = false;
    private FilterModel filterModel = new FilterModel();

    public AppStoreListIterator(IAppStore appStore, boolean isTop) {
        this.appStore = appStore;
        this.isTop = isTop;
    }

    @Override
    public boolean hasNext() {
        return firstQuery;
    }

    @Override
    public List<App> next() {
        List<App> list = new ArrayList<>();
        if (!hasNext()) return list;
        try {
            List<AppModel> apps = isTop ? appStore.getTopApps() : appStore.getApps();
            for (AppModel item : apps) {
                addApp(list, item);
            }
            firstQuery = false;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return list;
    }

    @Override
    public void setFilterEnabled(boolean filterEnabled) {
        this.filterEnabled = filterEnabled;
    }

    @Override
    public void setFilterModel(FilterModel filterModel) {
        this.filterModel = filterModel;
    }

    private boolean shouldSkip(AppModel app) {
        boolean skipByCategory = !filterModel.getAppCategoryModels().isEmpty();
        for (AppCategoryModel model : filterModel.getAppCategoryModels()) {
            if (model.getName().equals(app.getCategory())) {
                skipByCategory = false;
                break;
            }
        }
        return skipByCategory || (filterModel.isTop() && !app.isTop());
    }

    private void addApp(List<App> apps, AppModel app) {
        if (filterEnabled && shouldSkip(app)) {
            Log.i("Filtering out " + app.getPackageName());
        } else {
            apps.add(AppBuilder.build(app));
        }
    }
}
