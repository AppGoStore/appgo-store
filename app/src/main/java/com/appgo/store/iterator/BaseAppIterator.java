package com.appgo.store.iterator;

import com.appgo.store.model.App;
import com.appgo.store.model.FilterModel;

import java.util.Iterator;
import java.util.List;

public interface BaseAppIterator extends Iterator<List<App>> {

    void setFilterEnabled(boolean filterEnabled);

    void setFilterModel(FilterModel filterModel);
}
