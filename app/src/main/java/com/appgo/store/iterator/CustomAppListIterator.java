package com.appgo.store.iterator;

import com.appgo.store.model.App;
import com.appgo.store.model.AppBuilder;
import com.appgo.store.model.FilterModel;
import com.appgo.store.util.Log;
import com.dragons.aurora.playstoreapiv2.AppListIterator;
import com.dragons.aurora.playstoreapiv2.DocV2;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CustomAppListIterator implements BaseAppIterator {

    private boolean filterEnabled = false;
    private FilterModel filterModel = new FilterModel();
    private AppListIterator iterator;
    private List<String> relatedTags = new ArrayList<>();

    public CustomAppListIterator(AppListIterator iterator) {
        this.iterator = iterator;
    }

    public List<String> getRelatedTags() {
        Set<String> set = new HashSet<>(relatedTags);
        relatedTags.clear();
        relatedTags.addAll(set);
        return relatedTags;
    }

    @Override
    public void setFilterEnabled(boolean filterEnabled) {
        this.filterEnabled = filterEnabled;
    }

    @Override
    public void setFilterModel(FilterModel filterModel) {
        this.filterModel = filterModel;
    }

    @Override
    public List<App> next() {
        List<App> apps = new ArrayList<>();

        if (!iterator.hasNext())
            return apps;

        for (DocV2 docV2 : iterator.next()) {
            if (docV2.getDocType() == 53)
                relatedTags.add(docV2.getTitle());
            else if (docV2.getDocType() == 1) {
                addApp(apps, AppBuilder.build(docV2));
            } else {
                /*
                 * This one is either a movie, music or book, will exploit it at some point of time
                 * Movies = 6, Music = 2, Audio Books = 64
                 */
                continue;
            }
        }
        return apps;
    }

    @Override
    public boolean hasNext() {
        return iterator.hasNext();
    }

    private boolean shouldSkip(App app) {
        return (!filterModel.isPaidApps() && !app.isFree())
                || (!filterModel.isAppsWithAds() && app.isContainsAds())
                || (!filterModel.isGsfDependentApps() && !app.getDependencies().isEmpty())
                || (filterModel.getRating() > 0 && app.getRating().getAverage() < filterModel.getRating())
                || (filterModel.getDownloads() > 0 && app.getInstalls() < filterModel.getDownloads());
    }

    private void addApp(List<App> apps, App app) {
        if (filterEnabled && shouldSkip(app)) {
            Log.i("Filtering out " + app.getPackageName());
        } else {
            apps.add(app);
        }
    }
}
