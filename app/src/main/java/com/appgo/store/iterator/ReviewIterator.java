package com.appgo.store.iterator;

import com.appgo.store.model.Review;

import java.util.Iterator;
import java.util.List;

abstract public class ReviewIterator implements Iterator<List<Review>> {

    protected String packageName;

    protected int page = -1;

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }
}
