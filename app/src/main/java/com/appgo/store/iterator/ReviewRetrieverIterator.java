package com.appgo.store.iterator;

import com.appgo.store.AuroraApplication;
import com.appgo.store.exception.InvalidApiException;
import com.appgo.store.model.Review;
import com.appgo.store.model.ReviewBuilder;
import com.appgo.store.util.Log;
import com.dragons.aurora.playstoreapiv2.GooglePlayAPI;

import java.util.ArrayList;
import java.util.List;

public class ReviewRetrieverIterator extends ReviewIterator {

    static private final int PAGE_SIZE = 15;
    protected boolean hasNext = true;

    private GooglePlayAPI.REVIEW_SORT reviewSort = GooglePlayAPI.REVIEW_SORT.HIGHRATING;

    public void setReviewSort(GooglePlayAPI.REVIEW_SORT reviewSort) {
        this.reviewSort = reviewSort;
    }

    @Override
    public boolean hasNext() {
        return hasNext;
    }

    @Override
    public List<Review> next() {
        page++;
        List<Review> list = new ArrayList<>();
        try {
            list.addAll(getReviews(packageName, PAGE_SIZE * page, reviewSort));
            if (list.size() < PAGE_SIZE) {
                hasNext = false;
            }
        } catch (Exception e) {
            Log.i(e.getClass().getName() + ": " + e.getMessage());
        }
        return list;
    }

    private List<Review> getReviews(String packageId, int offset, GooglePlayAPI.REVIEW_SORT reviewSort) throws Exception {
        List<Review> reviews = new ArrayList<>();
        GooglePlayAPI api = AuroraApplication.api;
        if (api == null)
            throw new InvalidApiException();
        for (com.dragons.aurora.playstoreapiv2.Review review : api.reviews(
                packageId,
                reviewSort,
                offset,
                PAGE_SIZE
        ).getGetResponse().getReviewList()) {
            reviews.add(ReviewBuilder.build(review));
        }
        return reviews;
    }
}
