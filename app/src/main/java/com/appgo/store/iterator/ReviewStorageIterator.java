package com.appgo.store.iterator;


import com.appgo.store.model.Review;

import java.util.ArrayList;
import java.util.List;

public class ReviewStorageIterator extends ReviewIterator {

    static private final int PAGE_SIZE = 15;

    private List<Review> reviewList = new ArrayList<>();
    private ReviewRetrieverIterator iterator;

    private ReviewRetrieverIterator getRetrievingIterator() {
        if (null == iterator) {
            iterator = new ReviewRetrieverIterator();
            iterator.setPackageName(packageName);
        }
        return iterator;
    }

    public void setRetrievingIterator(ReviewRetrieverIterator iterator) {
        this.iterator = iterator;
    }

    @Override
    public boolean hasNext() {
        return reviewList.size() > (PAGE_SIZE * page) || getRetrievingIterator().hasNext();
    }

    @Override
    public List<Review> next() {
        page++;
        if (reviewList.size() < (PAGE_SIZE * (page + 1)) && getRetrievingIterator().hasNext()) {
            reviewList.addAll(getRetrievingIterator().next());
        }
        return current();
    }

    public boolean hasPrevious() {
        return page > 0;
    }

    public List<Review> previous() {
        page--;
        return current();
    }

    private List<Review> current() {
        int from = PAGE_SIZE * page;
        int to = from + PAGE_SIZE;
        return (from < 0 || to > reviewList.size()) ? new ArrayList<Review>() : reviewList.subList(from, to);
    }
}
