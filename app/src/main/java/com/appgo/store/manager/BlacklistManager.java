package com.appgo.store.manager;

import android.content.Context;

import com.appgo.store.Constants;
import com.appgo.store.util.PrefUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class BlacklistManager {

    private Context context;
    private Gson gson;

    public BlacklistManager(Context context) {
        this.context = context;
        this.gson = new Gson();
    }

    public void addToBlacklist(String packageName) {
        List<String> stringList = getBlacklistedPackages();
        if (!stringList.contains(packageName)) {
            stringList.add(packageName);
            saveBlacklist(stringList);
        }
    }

    public void removeFromBlacklist(String packageName) {
        List<String> stringList = getBlacklistedPackages();
        if (stringList.contains(packageName)) {
            stringList.remove(packageName);
            saveBlacklist(stringList);
        }
    }

    public boolean isBlacklisted(String packageName) {
        return getBlacklistedPackages().contains(packageName);
    }

    public void clear() {
        saveBlacklist(new ArrayList<>());
    }

    private void saveBlacklist(List<String> stringList) {
        PrefUtil.putString(context, Constants.PREFERENCE_BLACKLIST_PACKAGE_LIST, gson.toJson(stringList));
    }

    public List<String> getBlacklistedPackages() {
        String rawList = PrefUtil.getString(context, Constants.PREFERENCE_BLACKLIST_PACKAGE_LIST);
        Type type = new TypeToken<List<String>>() {
        }.getType();
        List<String> stringList = gson.fromJson(rawList, type);

        if (stringList == null)
            return new ArrayList<>();
        else
            return stringList;
    }
}
