package com.appgo.store.manager;

import android.content.Context;

import com.appgo.store.Constants;
import com.appgo.store.R;
import com.appgo.store.model.CategoryModel;
import com.appgo.store.util.PrefUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class CategoryManager {

    private Context context;
    private Gson gson;

    public CategoryManager(Context context) {
        this.context = context;
        this.gson = new Gson();
    }

    public static void clear(Context context) {
        PrefUtil.remove(context, Constants.CATEGORY_APPS);
        PrefUtil.remove(context, Constants.CATEGORY_GAME);
        PrefUtil.remove(context, Constants.CATEGORY_FAMILY);
    }

    public String getCategoryName(String categoryId) {
        if (null == categoryId) {
            return "";
        }
        if (categoryId.equals(Constants.TOP)) {
            return context.getString(R.string.title_all_apps);
        }
        return StringUtils.EMPTY;
    }

    public boolean fits(String appCategoryId, String chosenCategoryId) {
        return null == chosenCategoryId
                || chosenCategoryId.equals(Constants.TOP)
                || appCategoryId.equals(chosenCategoryId);
    }

    public boolean categoryListEmpty() {
        return PrefUtil.getString(context, Constants.CATEGORY_APPS).isEmpty();
    }

    public List<CategoryModel> getCategories(String categoryId) {
        return getCategoryById(categoryId);
    }

    public List<CategoryModel> getCategoryById(String categoryId) {
        Type type = new TypeToken<List<CategoryModel>>() {
        }.getType();
        String jsonString = PrefUtil.getString(context, categoryId);
        List<CategoryModel> categoryList = gson.fromJson(jsonString, type);
        if (categoryList == null || categoryList.isEmpty())
            return new ArrayList<>();
        else
            return categoryList;
    }
}
