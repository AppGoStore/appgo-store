package com.appgo.store.manager;

import android.content.Context;

import com.appgo.store.Constants;
import com.appgo.store.util.PrefUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class FavouritesManager {

    private Context context;
    private Gson gson;

    public FavouritesManager(Context context) {
        this.context = context;
        this.gson = new Gson();
    }

    public void addToFavourites(String packageName) {
        List<String> stringList = getFavouritePackages();
        if (!stringList.contains(packageName)) {
            stringList.add(packageName);
            saveFavourites(stringList);
        }
    }

    public void addToFavourites(List<String> packageNameList) {
        List<String> stringList = getFavouritePackages();
        for (String packageName : packageNameList) {
            if (!stringList.contains(packageName)) {
                stringList.add(packageName);
            }
        }
        saveFavourites(stringList);
    }

    public void removeFromFavourites(String packageName) {
        List<String> stringList = getFavouritePackages();
        if (stringList.contains(packageName)) {
            stringList.remove(packageName);
            saveFavourites(stringList);
        }
    }

    public boolean isFavourite(String packageName) {
        return getFavouritePackages().contains(packageName);
    }

    public void clear() {
        saveFavourites(new ArrayList<>());
    }

    private void saveFavourites(List<String> stringList) {
        PrefUtil.putString(context, Constants.PREFERENCE_FAVOURITE_PACKAGE_LIST, gson.toJson(stringList));
    }

    public List<String> getFavouritePackages() {
        String rawList = PrefUtil.getString(context, Constants.PREFERENCE_FAVOURITE_PACKAGE_LIST);
        Type type = new TypeToken<List<String>>() {
        }.getType();
        List<String> stringList = gson.fromJson(rawList, type);

        if (stringList == null)
            return new ArrayList<>();
        else
            return stringList;
    }
}
