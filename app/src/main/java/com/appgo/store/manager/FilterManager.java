package com.appgo.store.manager;

import android.content.Context;

import com.appgo.store.Constants;
import com.appgo.store.model.FilterModel;
import com.appgo.store.util.PrefUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.lang.reflect.Modifier;

public class FilterManager {

    public static FilterModel getFilterPreferences(Context context) {
        final Gson gson = new GsonBuilder().excludeFieldsWithModifiers(Modifier.TRANSIENT).create();
        final FilterModel filterModel = gson.fromJson(PrefUtil
                .getString(context, Constants.PREFERENCE_FILTER_APPS), FilterModel.class);
        if (filterModel == null) {
            FilterModel defaultModel = new FilterModel();
            PrefUtil.putString(context, Constants.PREFERENCE_FILTER_APPS, gson.toJson(defaultModel));
            return defaultModel;
        } else
            return filterModel;
    }

    public static void saveFilterPreferences(Context context, FilterModel filterModel) {
        final Gson gson = new GsonBuilder().excludeFieldsWithModifiers(Modifier.TRANSIENT).create();
        final String filterJSONString = gson.toJson(filterModel);
        PrefUtil.putString(context, Constants.PREFERENCE_FILTER_APPS, filterJSONString);
    }
}
