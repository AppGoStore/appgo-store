package com.appgo.store.manager;

import android.content.Context;

import com.appgo.store.Constants;
import com.appgo.store.util.PrefUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.HashMap;

public class IgnoreListManager {

    private Context context;
    private Gson gson;

    public IgnoreListManager(Context context) {
        this.context = context;
        this.gson = new Gson();
    }

    public void addToIgnoreList(String packageName, int versionCode) {
        final HashMap<String, Integer> ignoreMap = getIgnoreList();
        ignoreMap.remove(packageName);
        ignoreMap.put(packageName, versionCode);
        saveIgnoreList(ignoreMap);
    }

    public void removeFromIgnoreList(String packageName) {
        final HashMap<String, Integer> ignoreMap = getIgnoreList();
        ignoreMap.remove(packageName);
        saveIgnoreList(ignoreMap);
    }

    public boolean isIgnored(String packageName, Integer versionCode) {
        final HashMap<String, Integer> ignoreMap = getIgnoreList();
        if (ignoreMap.containsKey(packageName)) {
            Integer ignoredVersionCode = ignoreMap.get(packageName);
            return ignoredVersionCode != null && ignoredVersionCode.equals(versionCode);
        } else
            return false;
    }

    public void clear() {
        saveIgnoreList(new HashMap<>());
    }

    private void saveIgnoreList(HashMap<String, Integer> ignoreMap) {
        PrefUtil.putString(context, Constants.PREFERENCE_IGNORE_PACKAGE_LIST, gson.toJson(ignoreMap));
    }

    private HashMap<String, Integer> getIgnoreList() {
        String rawList = PrefUtil.getString(context, Constants.PREFERENCE_IGNORE_PACKAGE_LIST);
        Type type = new TypeToken<HashMap<String, Integer>>() {
        }.getType();

        HashMap<String, Integer> ignoreMap = gson.fromJson(rawList, type);
        if (ignoreMap == null)
            return new HashMap<>();
        else
            return ignoreMap;
    }
}
