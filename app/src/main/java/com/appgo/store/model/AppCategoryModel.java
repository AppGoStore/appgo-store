package com.appgo.store.model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import lombok.Data;

@Data
@Entity
public class AppCategoryModel {
    @PrimaryKey(autoGenerate = true)
    int id;

    private String name;
    private int priority;
}
