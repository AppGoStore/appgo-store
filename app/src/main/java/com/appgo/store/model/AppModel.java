package com.appgo.store.model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import lombok.Data;

@Data
@Entity
public class AppModel {
    @PrimaryKey(autoGenerate = true)
    int id;

    private String name;
    private String full_name;
    private String category;
    private String vendor;
    private String description;
    @SerializedName("app_link")
    private String appLink;
    @SerializedName("is_popular")
    private boolean isPopular;
    @SerializedName("is_visible")
    private boolean isVisible;
    @SerializedName("top")
    private boolean top;
    @SerializedName("icon_url")
    private String iconUrl;
    @SerializedName("url_appgallery")
    private String urlAppgallery;
    @SerializedName("url_weblink")
    private String urlWeblink;
    @SerializedName("url_3rd")
    private String url3rd;
    @SerializedName("category_top")
    private boolean categoryTop;
    private String faq;
    @SerializedName("package")
    private String packageName;
    @SerializedName("version_code")
    private int versionCode;
    @SerializedName("offer_type")
    private int offerType;
}