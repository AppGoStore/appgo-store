package com.appgo.store.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Data;

@Data
public class AppResponse {
    @SerializedName("search_webview")
    private String searchWebview;
    @SerializedName("search_keyword")
    private String searchKeyword;
    private List<AppModel> apps;
    private List<AppCategoryModel> categories;
}
