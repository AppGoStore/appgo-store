package com.appgo.store.model;

import com.appgo.store.Constants;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class FilterModel {
    private boolean systemApps = false;
    private boolean appsWithAds = true;
    private boolean paidApps = true;
    private boolean gsfDependentApps = true;
    private String category = Constants.TOP;
    private float rating = 0.0f;
    private int downloads = 0;
    private List<AppCategoryModel> appCategoryModels = new ArrayList<>();
    private boolean isTop = false;
}
