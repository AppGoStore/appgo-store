package com.appgo.store.model;

import android.content.pm.ApplicationInfo;

import lombok.Data;

@Data
public class ImageSource {

    private String url;
    private transient ApplicationInfo applicationInfo;

    ImageSource() {
    }

    ImageSource(String url) {
        setUrl(url);
    }
}
