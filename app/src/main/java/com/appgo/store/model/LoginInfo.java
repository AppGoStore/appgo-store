package com.appgo.store.model;

import android.content.Context;
import android.text.TextUtils;

import com.appgo.store.util.Accountant;
import com.appgo.store.util.PrefUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.lang.reflect.Modifier;
import java.util.Locale;

import lombok.Data;

@Data
public class LoginInfo {

    private String email;
    private String userName;
    private String userPicUrl;
    private String aasToken;
    private String gsfId;
    private String authToken;
    private String locale;
    private String tokenDispenserUrl;
    private String deviceDefinitionName;
    private String deviceDefinitionDisplayName;
    private String deviceCheckinConsistencyToken;
    private String deviceConfigToken;
    private String dfeCookie;

    public static void save(Context context, LoginInfo loginInfo) {
        Gson gson = new GsonBuilder().excludeFieldsWithModifiers(Modifier.TRANSIENT).create();
        String loginString = gson.toJson(loginInfo, LoginInfo.class);
        PrefUtil.putBoolean(context, Accountant.LOGGED_IN, true);
        PrefUtil.putString(context, Accountant.DATA, loginString);
    }

    public static LoginInfo getSavedInstance(Context context) {
        Gson gson = new GsonBuilder().excludeFieldsWithModifiers(Modifier.TRANSIENT).create();
        String loginString = PrefUtil.getString(context, Accountant.DATA);
        LoginInfo loginInfo = gson.fromJson(loginString, LoginInfo.class);
        return loginInfo == null ? new LoginInfo() : loginInfo;
    }

    public static void removeSavedInstance(Context context) {
        PrefUtil.putString(context, Accountant.DATA, "");
    }

    public Locale getLocale() {
        return TextUtils.isEmpty(locale) ? Locale.getDefault() : new Locale(locale);
    }

    public boolean isEmpty() {
        return email.isEmpty() && authToken.isEmpty();
    }
}
