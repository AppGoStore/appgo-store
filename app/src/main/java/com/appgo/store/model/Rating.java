package com.appgo.store.model;

import lombok.Data;

@Data
public class Rating {

    private float average;
    private int[] stars = new int[5];

    public int getStars(int starNum) {
        return stars[starNum - 1];
    }

    public void setStars(int starNum, int ratings) {
        stars[starNum - 1] = ratings;
    }
}
