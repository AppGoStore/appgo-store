package com.appgo.store.model;

import org.apache.commons.lang3.StringUtils;

import lombok.Data;

@Data
public class Review {

    private int rating;
    private String title;
    private String comment;

    private String userName;
    private String userPhotoUrl;
    private long timeStamp;

    public void setUserName(String userName) {
        this.userName = StringUtils.capitalize(userName);
    }
}
