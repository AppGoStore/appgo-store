package com.appgo.store.model.items;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.appgo.store.GlideApp;
import com.appgo.store.R;
import com.appgo.store.model.App;
import com.appgo.store.util.PackageUtil;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.google.android.material.checkbox.MaterialCheckBox;
import com.mikepenz.fastadapter.FastAdapter;
import com.mikepenz.fastadapter.items.AbstractItem;
import com.mikepenz.fastadapter.listeners.ClickEventHook;
import com.mikepenz.fastadapter.select.SelectExtension;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BulkItem extends AbstractItem<BulkItem.ViewHolder> {
    private App app;
    private String packageName;
    private boolean checked;

    public BulkItem(App app) {
        this.app = app;
        this.packageName = app.getPackageName();
    }

    @Override
    public int getLayoutRes() {
        return R.layout.item_checkbox;
    }

    @NotNull
    @Override
    public BulkItem.ViewHolder getViewHolder(@NotNull View view) {
        return new BulkItem.ViewHolder(view);
    }

    @Override
    public int getType() {
        return R.id.fastadapter_item;
    }

    public static class ViewHolder extends FastAdapter.ViewHolder<BulkItem> {
        @BindView(R.id.img)
        ImageView img;
        @BindView(R.id.line1)
        TextView line1;
        @BindView(R.id.line2)
        TextView line2;
        @BindView(R.id.checkbox)
        MaterialCheckBox checkBox;

        private Context context;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            context = itemView.getContext();
        }

        @Override
        public void bindView(@NotNull BulkItem item, @NotNull List<?> list) {
            final App app = item.getApp();

            line1.setText(app.getDisplayName());
            boolean isInstalled = PackageUtil.isInstalled(context, app);
            line2.setText(context.getText(isInstalled
                    ? R.string.list_installed
                    : R.string.list_not_installd));
            checkBox.setChecked(item.checked);
            checkBox.setVisibility(isInstalled
                    ? View.INVISIBLE
                    : View.VISIBLE);

            GlideApp
                    .with(context)
                    .load(app.getIconUrl())
                    .transition(new DrawableTransitionOptions().crossFade())
                    .transforms(new CenterCrop(), new RoundedCorners(30))
                    .into(img);

        }

        @Override
        public void unbindView(@NotNull BulkItem item) {
            line1.setText(null);
            line2.setText(null);
        }
    }

    public static final class CheckBoxClickEvent extends ClickEventHook<BulkItem> {
        @Nullable
        public View onBind(@NotNull RecyclerView.ViewHolder viewHolder) {
            return viewHolder instanceof BulkItem.ViewHolder
                    ? ((BulkItem.ViewHolder) viewHolder).checkBox
                    : null;
        }

        @Override
        public void onClick(@NotNull View view, int position, @NotNull FastAdapter<BulkItem> fastAdapter, @NotNull BulkItem item) {
            SelectExtension<BulkItem> selectExtension = fastAdapter.getExtension(SelectExtension.class);
            if (selectExtension != null) {
                selectExtension.toggleSelection(position);
                item.checked = !item.checked;
            }
        }
    }
}
