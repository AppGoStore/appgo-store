package com.appgo.store.network;

import com.appgo.store.BuildConfig;
import com.appgo.store.model.AppResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.lang.reflect.Modifier;
import java.util.Objects;

import okhttp3.Credentials;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class AppGalleryApi {

    private static final String APPS_URL = "https://appgo.life/files/new/apps_RU.json";
    private static final String OOBE_URL = "https://appgo.life/files/new/apps_RU_oobe.json";
    private static final String OOBE_PLAY_URL = "https://appgo.life/files/new/apps_RU_oobe_play.json";

    private OkHttpClient client;

    public AppGalleryApi() {
        client = new OkHttpClient.Builder()
                .addInterceptor(new BasicAuthInterceptor(BuildConfig.LOGIN, BuildConfig.PASSWORD))
                .build();
    }

    public AppResponse loadAppGalleryApps() throws IOException {
        Request request = new Request.Builder()
                .url(APPS_URL)
                .build();
        Response response = client.newCall(request).execute();
        String json = Objects.requireNonNull(response.body()).string();
        Gson gson = new GsonBuilder().excludeFieldsWithModifiers(Modifier.TRANSIENT).create();
        return gson.fromJson(json, AppResponse.class);
    }

    public AppResponse loadOobeApps() throws IOException {
        Request request = new Request.Builder()
                .url(OOBE_URL)
                .build();
        Response response = client.newCall(request).execute();
        String json = Objects.requireNonNull(response.body()).string();
        Gson gson = new GsonBuilder().excludeFieldsWithModifiers(Modifier.TRANSIENT).create();
        return gson.fromJson(json, AppResponse.class);
    }

    public AppResponse loadOobePlayApps() throws IOException {
        Request request = new Request.Builder()
                .url(OOBE_PLAY_URL)
                .build();
        Response response = client.newCall(request).execute();
        String json = Objects.requireNonNull(response.body()).string();
        Gson gson = new GsonBuilder().excludeFieldsWithModifiers(Modifier.TRANSIENT).create();
        return gson.fromJson(json, AppResponse.class);
    }

    private static class BasicAuthInterceptor implements Interceptor {

        private static final String HEADER_AUTH = "Authorization";
        private String credentials;

        public BasicAuthInterceptor(String user, String password) {
            this.credentials = Credentials.basic(user, password);
        }

        @NotNull
        @Override
        public Response intercept(Chain chain) throws IOException {
            Request request = chain.request();
            Request authenticatedRequest = request.newBuilder()
                    .header(HEADER_AUTH, credentials).build();
            return chain.proceed(authenticatedRequest);
        }

    }
}
