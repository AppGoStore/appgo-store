package com.appgo.store.notification;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.appgo.store.Constants;
import com.appgo.store.R;
import com.appgo.store.util.NotificationUtil;

public class QuickNotification {

    private static final int QUICK_NOTIFICATION_CHANNEL_ID = 69;
    private Context context;

    private QuickNotification(Context context) {
        this.context = context;
    }

    public static QuickNotification show(@NonNull Context context,
                                         @NonNull String contentTitle,
                                         @NonNull String contentText,
                                         @Nullable PendingIntent contentIntent) {
        QuickNotification quickNotification = new QuickNotification(context);
        quickNotification.show(contentTitle, contentText, contentIntent);
        return quickNotification;
    }

    public void show(String contentTitle, String contentText, PendingIntent contentIntent) {
        if (NotificationUtil.isNotificationEnabled(context)) {
            final NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            final NotificationCompat.Builder builder = new NotificationCompat.Builder(context, Constants.NOTIFICATION_CHANNEL_ALERT)
                    .setAutoCancel(true)
                    .setColor(context.getResources().getColor(R.color.colorAccent))
                    .setContentTitle(contentTitle)
                    .setContentText(contentText)
                    .setOnlyAlertOnce(true)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    .setSmallIcon(R.drawable.ic_notification_outlined);

            if (contentIntent != null)
                builder.setContentIntent(contentIntent);

            if (notificationManager != null)
                notificationManager.notify(QUICK_NOTIFICATION_CHANNEL_ID, builder.build());
        }
    }
}
