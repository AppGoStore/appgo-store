package com.appgo.store.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.appgo.store.Constants;
import com.appgo.store.util.Util;

public class BootReceiver extends BroadcastReceiver {

    static private int getUpdateInterval(Context context) {
        return Util.parseInt(Util.getPrefs(context)
                .getString(Constants.PREFERENCE_UPDATES_INTERVAL, "-1"), -1);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (!Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
            return;
        }
        Util.setUpdatesInterval(context.getApplicationContext(), getUpdateInterval(context));
    }
}