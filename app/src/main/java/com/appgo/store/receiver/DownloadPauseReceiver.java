package com.appgo.store.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.appgo.store.download.DownloadManager;

import static com.appgo.store.service.NotificationService.FETCH_GROUP_ID;

public class DownloadPauseReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle extras = intent.getExtras();
        if ((extras != null)) {
            final int groupId = extras.getInt(FETCH_GROUP_ID, -1);
            DownloadManager.getFetchInstance(context).pauseGroup(groupId);
        }
    }
}
