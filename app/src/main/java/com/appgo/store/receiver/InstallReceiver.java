package com.appgo.store.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.appgo.store.AuroraApplication;
import com.appgo.store.Constants;

public class InstallReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle extras = intent.getExtras();
        if ((extras != null)) {
            final String packageName = extras.getString(Constants.INTENT_PACKAGE_NAME, "");
            final String versionString = extras.getString(Constants.DOWNLOAD_VERSION_CODE);
            if (!packageName.isEmpty() && versionString != null) {
                AuroraApplication.getInstaller().installSplit(packageName, Integer.parseInt(versionString));
            }
        }
    }
}
