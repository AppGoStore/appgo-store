package com.appgo.store.sheet;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.appgo.store.R;
import com.appgo.store.manager.FilterManager;
import com.appgo.store.model.AppCategoryModel;
import com.appgo.store.model.FilterModel;
import com.appgo.store.task.AppGalleryTask;
import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class AppFilterBottomSheet extends BaseBottomSheet {

    @BindView(R.id.filter_all)
    Chip filterAll;
    @BindView(R.id.filter_top)
    Chip filterTop;
    @BindView(R.id.categories_chips)
    ChipGroup categoriesChips;

    private FilterModel filterModel;

    @NonNull
    @Override
    public View onCreateContentView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.sheet_app_filter, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    protected void onContentViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onContentViewCreated(view, savedInstanceState);
        filterModel = FilterManager.getFilterPreferences(requireContext());
        setupSingleChips();
        Observable.fromCallable(() -> new AppGalleryTask()
                .getCategories())
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(this::setupMultipleChips)
                .doOnError(Throwable::printStackTrace)
                .subscribe();
    }

    @OnClick(R.id.btn_positive)
    public void applyFilter() {
        FilterManager.saveFilterPreferences(requireContext(), filterModel);
        dismissAllowingStateLoss();
    }

    @OnClick(R.id.btn_negative)
    public void closeFilter() {
        dismissAllowingStateLoss();
    }

    private void setupSingleChips() {
        filterAll.setChecked(!filterModel.isTop());
        filterTop.setChecked(filterModel.isTop());
        filterAll.setOnCheckedChangeListener((v, isChecked) -> filterModel.setTop(!isChecked));
        filterTop.setOnCheckedChangeListener((v, isChecked) -> filterModel.setTop(isChecked));
    }

    private void setupMultipleChips(List<AppCategoryModel> categories) {
        for (AppCategoryModel model : categories) {
            Chip chip = new Chip(requireContext());
            chip.setId(model.getId());
            chip.setText(model.getName());
            chip.setChecked(filterModel.getAppCategoryModels().contains(model));
            chip.setOnCheckedChangeListener((buttonView, isChecked) -> {
                List<Integer> ids = categoriesChips.getCheckedChipIds();
                List<AppCategoryModel> models = new ArrayList<>();
                for (AppCategoryModel item : categories) {
                    if (ids.contains(item.getId())) models.add(item);
                }
                filterModel.setAppCategoryModels(models);
            });
            categoriesChips.addView(chip);
        }
    }
}
