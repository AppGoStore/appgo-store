package com.appgo.store.task;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import com.appgo.store.Constants;
import com.appgo.store.util.CertUtil;
import com.appgo.store.util.Util;

import java.util.ArrayList;
import java.util.List;

public class AllAppsTask {

    private Context context;
    private PackageManager packageManager;
    private boolean fDroidFilterEnabled;
    private boolean isExtendedUpdatesEnabled;

    public AllAppsTask(Context context) {
        this.context = context;
        this.packageManager = context.getPackageManager();
        this.fDroidFilterEnabled = Util.filterFDroidAppsEnabled(context);
        this.isExtendedUpdatesEnabled = Util.isExtendedUpdatesEnabled(context);
    }

    public PackageManager getPackageManager() {
        return packageManager;
    }

    public Context getContext() {
        return context;
    }

    List<String> getLocalInstalledApps() {
        List<String> packageList = new ArrayList<>();
        PackageManager packageManager = context.getPackageManager();
        for (PackageInfo packageInfo : packageManager.getInstalledPackages(PackageManager.GET_META_DATA)) {
            final String packageName = packageInfo.packageName;
            if (packageInfo.applicationInfo != null
                    && !packageInfo.applicationInfo.enabled
                    && !isExtendedUpdatesEnabled)
                continue;
            String packageInstaller = packageManager.getInstallerPackageName(packageName);
            if (fDroidFilterEnabled && packageInstaller != null
                    && packageInstaller.equals(Constants.PRIVILEGED_EXTENSION_PACKAGE_NAME_FDROID))
                continue;
            if (fDroidFilterEnabled && CertUtil.isFDroidApp(context, packageName))
                continue;
            packageList.add(packageName);
        }
        return packageList;
    }
}
