package com.appgo.store.task;

import android.content.Context;

import com.appgo.store.model.App;
import com.appgo.store.model.AppBuilder;
import com.appgo.store.model.AppModel;
import com.appgo.store.util.PackageUtil;
import com.dragons.aurora.playstoreapiv2.DetailsResponse;
import com.dragons.aurora.playstoreapiv2.GooglePlayAPI;

public class AppDetailTask {

    private Context context;
    private GooglePlayAPI api;
    private IAppStore appStore;

    public AppDetailTask(Context context, GooglePlayAPI api) {
        this.context = context;
        this.api = api;
        this.appStore = new AppGalleryTask();
    }

    public App getInfo(String packageName) throws Exception {
        AppModel appModel = appStore.getAppByPackageName(packageName);
        App app;
        if (appModel != null) {
            app = AppBuilder.build(appModel);
        } else {
            final DetailsResponse response = api.details(packageName);
            app = AppBuilder.build(response);
        }
        if (PackageUtil.isInstalled(context, app))
            app.setInstalled(true);
        return app;
    }
}