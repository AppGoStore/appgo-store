package com.appgo.store.task;

import com.appgo.store.AuroraApplication;
import com.appgo.store.model.AppCategoryModel;
import com.appgo.store.model.AppModel;
import com.appgo.store.model.AppResponse;
import com.appgo.store.network.AppGalleryApi;

import java.io.IOException;
import java.util.List;

public class AppGalleryTask implements IAppStore {

    @Override
    public List<AppModel> getApps() {
        return AuroraApplication.getDatabase().applications().getAll();
    }

    @Override
    public List<AppModel> getOobeApps() throws IOException {
        AppGalleryApi appGalleryApi = new AppGalleryApi();
        AppResponse response = appGalleryApi.loadOobeApps();
        return response.getApps();
    }

    @Override
    public List<AppModel> getOobePlayApps() throws IOException {
        AppGalleryApi appGalleryApi = new AppGalleryApi();
        AppResponse response = appGalleryApi.loadOobePlayApps();
        return response.getApps();
    }

    @Override
    public List<AppModel> getTopApps() {
        return AuroraApplication.getDatabase().applications().getTop();
    }

    @Override
    public AppModel getAppByPackageName(String packageName) {
        return AuroraApplication.getDatabase().applications().getApp(packageName);
    }

    @Override
    public List<AppCategoryModel> getCategories() {
        return AuroraApplication.getDatabase().categories().getAll();
    }

}