package com.appgo.store.task;

import android.content.Context;
import android.content.ContextWrapper;

import com.appgo.store.model.App;
import com.appgo.store.util.ApiBuilderUtil;
import com.dragons.aurora.playstoreapiv2.GooglePlayAPI;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public abstract class BaseTask extends ContextWrapper {

    protected Context context;
    protected GooglePlayAPI api;

    public BaseTask(Context context) {
        super(context);
        this.context = context;
    }

    public GooglePlayAPI getApi() throws Exception {
        return ApiBuilderUtil.getApi(context);
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public List<App> filterGoogleApps(List<App> apps) {
        Set<String> gAppsSet = new HashSet<>();
        gAppsSet.add("com.chrome.beta");
        gAppsSet.add("com.chrome.canary");
        gAppsSet.add("com.chrome.dev");
        gAppsSet.add("com.android.chrome");
        gAppsSet.add("com.niksoftware.snapseed");
        gAppsSet.add("com.google.toontastic");

        List<App> appList = new ArrayList<>();
        for (App app : apps) {
            if (!app.getPackageName().startsWith("com.google") && !gAppsSet.contains(app.getPackageName())) {
                appList.add(app);
            }
        }
        return appList;
    }
}