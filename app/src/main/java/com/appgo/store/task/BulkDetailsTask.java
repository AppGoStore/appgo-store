package com.appgo.store.task;

import com.appgo.store.model.App;
import com.appgo.store.model.AppBuilder;
import com.appgo.store.model.AppModel;
import com.dragons.aurora.playstoreapiv2.BulkDetailsEntry;
import com.dragons.aurora.playstoreapiv2.GooglePlayAPI;

import java.util.ArrayList;
import java.util.List;

public class BulkDetailsTask {

    private GooglePlayAPI api;
    private IAppStore appStore = new AppGalleryTask();

    public BulkDetailsTask(GooglePlayAPI api) {
        this.api = api;
    }

    public List<App> getRemoteAppList(List<String> packageNames) throws Exception {
        List<App> apps = new ArrayList<>();
        List<String> googlePlayPackageNames = new ArrayList<>();
        for (String packageName : packageNames) {
            AppModel appModel = appStore.getAppByPackageName(packageName);
            if (appModel != null) {
                apps.add(AppBuilder.build(appModel));
            } else {
                googlePlayPackageNames.add(packageName);
            }
        }
        if (!googlePlayPackageNames.isEmpty()) {
            for (BulkDetailsEntry details : api.bulkDetails(googlePlayPackageNames).getEntryList()) {
                if (!details.hasDoc()) {
                    continue;
                }
                apps.add(AppBuilder.build(details.getDoc()));
            }
        }
        return apps;
    }
}
