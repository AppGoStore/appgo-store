package com.appgo.store.task;

import android.content.Context;

import androidx.annotation.NonNull;

import com.appgo.store.iterator.BaseAppIterator;
import com.appgo.store.model.App;
import com.appgo.store.util.Util;

import java.util.ArrayList;
import java.util.List;

public class CategoryAppsTask extends BaseTask {

    public CategoryAppsTask(Context context) {
        super(context);
    }

    public List<App> getApps(@NonNull BaseAppIterator iterator) {
        if (!iterator.hasNext()) {
            return new ArrayList<>();
        }
        return new ArrayList<>(getNextBatch(iterator));
    }

    public List<App> getNextBatch(BaseAppIterator iterator) {
        List<App> apps = new ArrayList<>(iterator.next());
        if (Util.filterGoogleAppsEnabled(context))
            return filterGoogleApps(apps);
        else
            return apps;
    }
}
