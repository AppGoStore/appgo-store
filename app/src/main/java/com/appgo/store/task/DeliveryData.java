package com.appgo.store.task;

import android.content.Context;

import com.appgo.store.Constants;
import com.appgo.store.exception.AppNotFoundException;
import com.appgo.store.exception.NotPurchasedException;
import com.appgo.store.model.App;
import com.appgo.store.util.Log;
import com.appgo.store.util.PrefUtil;
import com.dragons.aurora.playstoreapiv2.AndroidAppDeliveryData;
import com.dragons.aurora.playstoreapiv2.BuyResponse;
import com.dragons.aurora.playstoreapiv2.DeliveryResponse;
import com.dragons.aurora.playstoreapiv2.GooglePlayAPI;

import java.io.IOException;

public class DeliveryData extends BaseTask {

    public AndroidAppDeliveryData deliveryData;
    private GooglePlayAPI api;
    private String downloadToken;

    public DeliveryData(Context context) {
        super(context);
    }

    public AndroidAppDeliveryData getDeliveryData(App app) throws Exception {
        api = getApi();
        purchase(app);
        delivery(app);
        return deliveryData;
    }

    public void purchase(App app) {
        try {
            BuyResponse buyResponse = api.purchase(app.getPackageName(), app.getVersionCode(), app.getOfferType());
            if (buyResponse.hasPurchaseStatusResponse()
                    && buyResponse.getPurchaseStatusResponse().hasAppDeliveryData()
                    && buyResponse.getPurchaseStatusResponse().getAppDeliveryData().hasDownloadUrl()) {
                deliveryData = buyResponse.getPurchaseStatusResponse().getAppDeliveryData();
            }
            if (buyResponse.hasDownloadToken()) {
                downloadToken = buyResponse.getDownloadToken();
            }
        } catch (Exception e) {
            Log.d("Failed to purchase %s", app.getDisplayName());
        }
    }

    void delivery(App app) throws IOException {
        DeliveryResponse deliveryResponse = api.delivery(
                app.getPackageName(),
                shouldDownloadDelta(app) ? app.getInstalledVersionCode() : 0,
                app.getVersionCode(),
                app.getOfferType(),
                downloadToken);
        if (deliveryResponse.hasAppDeliveryData()
                && deliveryResponse.getAppDeliveryData().hasDownloadUrl()) {
            deliveryData = deliveryResponse.getAppDeliveryData();
        } else if (deliveryData == null && deliveryResponse.hasStatus()) {
            handleError(app, deliveryResponse.getStatus());
        }
    }

    private void handleError(App app, int statusCode) throws IOException {
        switch (statusCode) {
            case 2:
                throw new AppNotFoundException(app.getDisplayName(), statusCode);
            case 3:
                throw new NotPurchasedException(app.getDisplayName(), statusCode);
        }
    }

    private boolean shouldDownloadDelta(App app) {
        return PrefUtil.getBoolean(context, Constants.PREFERENCE_DOWNLOAD_DELTAS)
                && app.getInstalledVersionCode() < app.getVersionCode();
    }
}
