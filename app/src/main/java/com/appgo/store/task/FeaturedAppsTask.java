package com.appgo.store.task;

import android.content.Context;

import com.appgo.store.Constants;
import com.appgo.store.iterator.AppStoreListIterator;
import com.appgo.store.iterator.BaseAppIterator;
import com.appgo.store.iterator.CustomAppListIterator;
import com.appgo.store.model.App;
import com.appgo.store.util.Util;
import com.dragons.aurora.playstoreapiv2.CategoryAppsIterator;
import com.dragons.aurora.playstoreapiv2.GooglePlayAPI;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FeaturedAppsTask extends BaseTask {

    private IAppStore appStore;

    public FeaturedAppsTask(Context context) {
        super(context);
        this.appStore = new AppGalleryTask();
    }

    public List<App> getApps(GooglePlayAPI api, String categoryId, GooglePlayAPI.SUBCATEGORY subCategory) {
        BaseAppIterator iterator;
        if (categoryId.equals(Constants.CATEGORY_APP_GALLERY)) {
            iterator = new AppStoreListIterator(appStore, true);
        } else {
            iterator = new CustomAppListIterator(new CategoryAppsIterator(api, categoryId, subCategory));
        }
        List<App> apps = new ArrayList<>(iterator.next());

        if (!categoryId.equals(Constants.CATEGORY_APP_GALLERY)) {
            apps = new ArrayList<>(filterAppGalleryApps(apps));
        }

        if (Util.filterGoogleAppsEnabled(context))
            return filterGoogleApps(apps);
        else
            return apps;
    }

    private List<App> filterAppGalleryApps(List<App> apps) {
        List<App> list = new ArrayList<>();
        for (App app : apps) {
            try {
                if (appStore.getAppByPackageName(app.getPackageName()) == null) list.add(app);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return list;
    }
}
