package com.appgo.store.task;

import android.content.Context;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.SystemClock;

import com.appgo.store.Constants;
import com.appgo.store.util.PrefUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static android.content.Context.LOCATION_SERVICE;

public class GeoSpoofTask {

    private LocationManager locationManager;
    private String locationProvider;
    private Location mockLocation;
    private Context context;

    public GeoSpoofTask(Context context) {
        this.context = context;
        this.locationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);
        this.locationProvider = LocationManager.GPS_PROVIDER;
        this.mockLocation = new Location(locationProvider);
    }

    public boolean spoof(String geoLocation) throws Exception {
        final List<Address> addresses = getAddress(geoLocation);
        if (!addresses.isEmpty()) {
            spoofLocation(addresses.get(0).getLatitude(), addresses.get(0).getLongitude());
            PrefUtil.putString(context, Constants.PREFERENCE_SPOOF_GEOLOCATION, geoLocation);
            return true;
        } else {
            return false;
        }
    }

    private List<Address> getAddress(String geoLocation) throws Exception {
        final Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        final List<Address> AllAddress = geocoder.getFromLocationName(geoLocation, 5);
        final List<Address> ValidAddress = new ArrayList<>(AllAddress.size());
        for (Address address : AllAddress) {
            if (address.hasLatitude() && address.hasLongitude()) {
                ValidAddress.add(address);
            }
        }
        return ValidAddress;
    }

    private void spoofLocation(double latitude, double longitude) {
        mockLocation.setLatitude(latitude);
        mockLocation.setLongitude(longitude);
        mockLocation.setTime(System.currentTimeMillis());
        mockLocation.setAccuracy(Criteria.ACCURACY_HIGH);
        mockLocation.setElapsedRealtimeNanos(SystemClock.elapsedRealtimeNanos());

        locationManager.addTestProvider(locationProvider,
                true,
                true,
                true,
                false,
                false,
                false,
                true,
                Criteria.POWER_MEDIUM,
                Criteria.ACCURACY_HIGH);
        locationManager.setTestProviderEnabled(locationProvider, true);
        locationManager.setTestProviderLocation(locationProvider, mockLocation);
    }
}
