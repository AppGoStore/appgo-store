package com.appgo.store.task;

import com.appgo.store.model.AppCategoryModel;
import com.appgo.store.model.AppModel;

import java.io.IOException;
import java.util.List;

public interface IAppStore {

	List<AppModel> getApps() throws IOException;

	List<AppModel> getOobeApps() throws IOException;

	List<AppModel> getOobePlayApps() throws IOException;

	List<AppModel> getTopApps() throws IOException;

	AppModel getAppByPackageName(String packageName) throws IOException;

	List<AppCategoryModel> getCategories() throws IOException;
}
