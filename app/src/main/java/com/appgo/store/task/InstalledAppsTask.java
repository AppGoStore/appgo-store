package com.appgo.store.task;

import android.content.Context;
import android.text.TextUtils;

import com.appgo.store.model.App;
import com.appgo.store.util.PackageUtil;

import java.util.ArrayList;
import java.util.List;

public class InstalledAppsTask extends AllAppsTask {


    public InstalledAppsTask(Context context) {
        super(context);
    }

    public List<App> getInstalledApps() {
        List<App> appList = new ArrayList<>();
        for (App app : getAllLocalApps()) {
            if ((getPackageManager().getLaunchIntentForPackage(app.getPackageName())) != null)
                appList.add(app);
        }
        return appList;
    }

    public List<App> getAllLocalApps() {
        List<App> appList = new ArrayList<>();
        List<String> packageList = getLocalInstalledApps();
        for (String packageName : packageList) {

            if (TextUtils.isEmpty(packageName)) {
                continue;
            }

            final App app = PackageUtil.getAppFromPackageName(getPackageManager(), packageName, true);
            appList.add(app);
        }
        return appList;
    }
}