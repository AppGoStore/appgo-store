package com.appgo.store.task;

import com.appgo.store.iterator.ReviewStorageIterator;
import com.appgo.store.model.Review;

import java.util.ArrayList;
import java.util.List;

public class ReviewsHelper {

    private List<Review> reviewList = new ArrayList<>();
    private ReviewStorageIterator iterator;

    public ReviewsHelper(ReviewStorageIterator iterator) {
        this.iterator = iterator;
    }

    public void setIterator(ReviewStorageIterator iterator) {
        this.iterator = iterator;
    }

    public List<Review> getReviews() {
        reviewList.clear();
        reviewList.addAll(iterator.next());
        return reviewList;
    }
}