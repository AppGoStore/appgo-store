package com.appgo.store.task;

import android.content.Context;

import com.appgo.store.exception.InvalidApiException;
import com.appgo.store.iterator.CustomAppListIterator;
import com.appgo.store.model.App;
import com.appgo.store.util.Util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SearchTask {

    private Context context;

    public SearchTask(Context context) {
        this.context = context;
    }

    public List<App> getSearchResults(CustomAppListIterator iterator) throws Exception {
        if (iterator == null)
            throw new InvalidApiException();
        if (!iterator.hasNext()) {
            return new ArrayList<>();
        }
        return new ArrayList<>(getNextBatch(iterator));
    }

    private List<App> getNextBatch(CustomAppListIterator iterator) {
        List<App> apps = new ArrayList<>();

        if (iterator.hasNext())
            apps.addAll(iterator.next());

        if (iterator.hasNext())
            apps.addAll(iterator.next());

        if (apps.isEmpty())
            return apps;

        if (Util.filterGoogleAppsEnabled(context))
            return filterGoogleApps(apps);
        else
            return apps;
    }

    private List<App> filterGoogleApps(List<App> apps) {
        Set<String> gAppsSet = new HashSet<>();
        gAppsSet.add("com.chrome.beta");
        gAppsSet.add("com.chrome.canary");
        gAppsSet.add("com.chrome.dev");
        gAppsSet.add("com.android.chrome");
        gAppsSet.add("com.niksoftware.snapseed");
        gAppsSet.add("com.google.toontastic");

        List<App> appList = new ArrayList<>();
        for (App app : apps) {
            if (!app.getPackageName().startsWith("com.google") && !gAppsSet.contains(app.getPackageName())) {
                appList.add(app);
            }
        }
        return appList;
    }
}
