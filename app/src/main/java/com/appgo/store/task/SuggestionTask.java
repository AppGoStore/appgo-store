package com.appgo.store.task;

import com.dragons.aurora.playstoreapiv2.GooglePlayAPI;
import com.dragons.aurora.playstoreapiv2.SearchSuggestEntry;

import java.util.List;

public class SuggestionTask {

    private GooglePlayAPI api;

    public SuggestionTask(GooglePlayAPI api) {
        this.api = api;
    }

    public List<SearchSuggestEntry> getSearchSuggestions(String query) throws Exception {
        return api.searchSuggest(query).getEntryList();
    }
}