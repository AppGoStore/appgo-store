package com.appgo.store.task;

import android.content.Context;
import android.text.TextUtils;

import com.appgo.store.exception.CredentialsEmptyException;
import com.appgo.store.exception.MalformedRequestException;
import com.appgo.store.manager.BlacklistManager;
import com.appgo.store.model.App;
import com.appgo.store.model.AppBuilder;
import com.appgo.store.util.Log;
import com.appgo.store.util.PackageUtil;
import com.dragons.aurora.playstoreapiv2.BulkDetailsEntry;
import com.dragons.aurora.playstoreapiv2.GooglePlayAPI;

import java.util.ArrayList;
import java.util.List;

public class UpdatableAppsTask extends AllAppsTask {

    private GooglePlayAPI api;

    public UpdatableAppsTask(GooglePlayAPI api, Context context) {
        super(context);
        this.api = api;
    }

    public List<App> getUpdatableApps() throws Exception {
        List<App> appList = new ArrayList<>();
        List<String> packageList = getLocalInstalledApps();
        packageList = filterBlacklistedApps(packageList);
        for (App app : getAppsFromPlayStore(packageList)) {
            final String packageName = app.getPackageName();
            if (TextUtils.isEmpty(packageName)) {
                continue;
            }

            final App installedApp = PackageUtil.getAppFromPackageName(getPackageManager(), packageName, false);
            app = addInstalledAppInfo(app, installedApp);
            if (installedApp != null && installedApp.getVersionCode() < app.getVersionCode()) {
                appList.add(app);
            }
        }
        return appList;
    }

    public List<App> getAppsFromPlayStore(List<String> packageNames) throws Exception {
        return new ArrayList<>(getRemoteAppList(packageNames));
    }

    private List<App> getRemoteAppList(List<String> packageNames) throws Exception {
        final List<App> appList = new ArrayList<>();
        try {
            final List<BulkDetailsEntry> bulkDetailsEntries = api.bulkDetails(packageNames).getEntryList();
            for (BulkDetailsEntry bulkDetailsEntry : bulkDetailsEntries) {
                if (!bulkDetailsEntry.hasDoc()) {
                    continue;
                }
                appList.add(AppBuilder.build(bulkDetailsEntry.getDoc()));
            }
        } catch (Exception e) {
            if (e instanceof MalformedRequestException) {
                Log.e("Malformed Request : %s", e.getMessage());
            } else if (e instanceof NullPointerException)
                throw new CredentialsEmptyException();
            else
                throw e;
        }
        return appList;
    }

    public App addInstalledAppInfo(App appFromMarket, App installedApp) {
        if (installedApp != null) {
            appFromMarket.setDisplayName(installedApp.getDisplayName());
            appFromMarket.setSystem(installedApp.isSystem());
        }
        return appFromMarket;
    }

    public List<String> filterBlacklistedApps(List<String> packageList) {
        packageList.removeAll(new BlacklistManager(getContext()).getBlacklistedPackages());
        return packageList;
    }
}