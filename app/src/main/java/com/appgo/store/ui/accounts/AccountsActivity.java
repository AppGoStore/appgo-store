package com.appgo.store.ui.accounts;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentTransaction;

import com.appgo.store.R;
import com.appgo.store.ui.accounts.fragment.AccountsFragment;
import com.appgo.store.ui.main.AuroraActivity;
import com.appgo.store.ui.preference.SettingsActivity;
import com.appgo.store.ui.single.activity.BaseActivity;
import com.appgo.store.util.ViewUtil;
import com.appgo.store.util.diff.NavigationUtil;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AccountsActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accounts);
        ButterKnife.bind(this);
        setupActionbar();

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content, new AccountsFragment())
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .commitAllowingStateLoss();
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home: {
                onBackPressed();
                return true;
            }
            case R.id.action_setting: {
                startActivity(new Intent(this, SettingsActivity.class),
                        ViewUtil.getEmptyActivityBundle(this));
                finishAfterTransition();
                return true;
            }
            case R.id.action_home: {
                startActivity(new Intent(this, AuroraActivity.class),
                        ViewUtil.getEmptyActivityBundle(this));
                finishAfterTransition();
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_intro, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        NavigationUtil.launchAuroraActivity(this);
        finish();
        super.onBackPressed();
    }

    private void setupActionbar() {
        setSupportActionBar(toolbar);
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setElevation(0f);
            actionBar.setTitle(getString(R.string.menu_account));
        }
    }
}
