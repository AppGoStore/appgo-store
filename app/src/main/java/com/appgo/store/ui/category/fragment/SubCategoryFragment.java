package com.appgo.store.ui.category.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.appgo.store.Constants;
import com.appgo.store.R;
import com.appgo.store.model.App;
import com.appgo.store.model.items.EndlessItem;
import com.appgo.store.sheet.AppMenuSheet;
import com.appgo.store.ui.category.CategoryAppsActivity;
import com.appgo.store.ui.category.CategoryAppsModel;
import com.appgo.store.ui.details.DetailsActivity;
import com.appgo.store.ui.single.fragment.BaseFragment;
import com.appgo.store.ui.view.ViewFlipper2;
import com.appgo.store.util.Util;
import com.appgo.store.util.ViewUtil;
import com.dragons.aurora.playstoreapiv2.GooglePlayAPI;
import com.mikepenz.fastadapter.FastAdapter;
import com.mikepenz.fastadapter.adapters.ItemAdapter;
import com.mikepenz.fastadapter.scroll.EndlessRecyclerOnScrollListener;
import com.mikepenz.fastadapter.ui.items.ProgressItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SubCategoryFragment extends BaseFragment implements
        SharedPreferences.OnSharedPreferenceChangeListener {

    @BindView(R.id.recycler)
    RecyclerView recyclerView;
    @BindView(R.id.view_flipper)
    ViewFlipper2 viewFlipper;

    private GooglePlayAPI.SUBCATEGORY subcategory = GooglePlayAPI.SUBCATEGORY.TOP_FREE;
    private SharedPreferences sharedPreferences;

    private CategoryAppsModel model;

    private FastAdapter fastAdapter;
    private ItemAdapter<EndlessItem> itemAdapter;
    private ItemAdapter<ProgressItem> progressItemAdapter;
    private EndlessRecyclerOnScrollListener endlessScrollListener;

    private GooglePlayAPI.SUBCATEGORY getSubcategory() {
        return subcategory;
    }

    private void setSubcategory(Bundle bundle) {
        String category = bundle.getString("SUBCATEGORY");
        if (category != null)
            switch (category) {
                case "TOP_FREE":
                    subcategory = GooglePlayAPI.SUBCATEGORY.TOP_FREE;
                    break;
                case "TOP_GROSSING":
                    subcategory = GooglePlayAPI.SUBCATEGORY.TOP_GROSSING;
                    break;
                default:
                    subcategory = GooglePlayAPI.SUBCATEGORY.MOVERS_SHAKERS;
            }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferences = Util.getPrefs(requireContext());
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_category_applist, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null)
            setSubcategory(bundle);
        setupRecycler();

        model = new ViewModelProvider(this).get(CategoryAppsModel.class);
        model.getCategoryApps().observe(getViewLifecycleOwner(), this::dispatchAppsToAdapter);
        model.fetchCategoryApps(CategoryAppsActivity.categoryId, getSubcategory(), false);
    }

    @Override
    public void onResume() {
        super.onResume();
        sharedPreferences.registerOnSharedPreferenceChangeListener(this);
        if (fastAdapter != null) fastAdapter.notifyAdapterDataSetChanged();
    }

    @Override
    public void onDestroy() {
        sharedPreferences.unregisterOnSharedPreferenceChangeListener(this);
        super.onDestroy();
    }

    private void dispatchAppsToAdapter(List<EndlessItem> endlessItemList) {
        itemAdapter.add(endlessItemList);
        recyclerView.post(() -> {
            progressItemAdapter.clear();
        });

        if (itemAdapter != null && itemAdapter.getAdapterItems().size() > 0) {
            viewFlipper.switchState(ViewFlipper2.DATA);
        } else {
            viewFlipper.switchState(ViewFlipper2.EMPTY);
        }
    }

    private void purgeAdapterData() {
        recyclerView.post(() -> {
            progressItemAdapter.clear();
            itemAdapter.clear();
            endlessScrollListener.resetPageCount();
        });
    }

    private void setupRecycler() {
        fastAdapter = new FastAdapter<>();
        itemAdapter = new ItemAdapter<>();
        progressItemAdapter = new ItemAdapter<>();

        fastAdapter.addAdapter(0, itemAdapter);
        fastAdapter.addAdapter(1, progressItemAdapter);

        fastAdapter.setOnClickListener((view, iAdapter, item, position) -> {
            if (item instanceof EndlessItem) {
                final App app = ((EndlessItem) item).getApp();
                final Intent intent = new Intent(requireContext(), DetailsActivity.class);
                intent.putExtra(Constants.INTENT_PACKAGE_NAME, app.getPackageName());
                intent.putExtra(Constants.STRING_EXTRA, gson.toJson(app));
                startActivity(intent, ViewUtil.getEmptyActivityBundle((AppCompatActivity) requireActivity()));
            }
            return false;
        });

        fastAdapter.setOnLongClickListener((view, iAdapter, item, position) -> {
            if (item instanceof EndlessItem) {
                final App app = ((EndlessItem) item).getApp();
                final AppMenuSheet menuSheet = new AppMenuSheet();
                final Bundle bundle = new Bundle();
                bundle.putInt(Constants.INT_EXTRA, Integer.parseInt(position.toString()));
                bundle.putString(Constants.STRING_EXTRA, gson.toJson(app));
                menuSheet.setArguments(bundle);
                menuSheet.show(getChildFragmentManager(), AppMenuSheet.TAG);
            }
            return true;
        });

        endlessScrollListener = new EndlessRecyclerOnScrollListener(progressItemAdapter) {
            @Override
            public void onLoadMore(int currentPage) {
                recyclerView.post(() -> {
                    progressItemAdapter.clear();
                    progressItemAdapter.add(new ProgressItem());
                });
                model.fetchCategoryApps(CategoryAppsActivity.categoryId, getSubcategory(), true);
            }
        };

        recyclerView.addOnScrollListener(endlessScrollListener);
        recyclerView.setLayoutManager(new LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(fastAdapter);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(Constants.PREFERENCE_FILTER_APPS)) {
            purgeAdapterData();
            model.fetchCategoryApps(CategoryAppsActivity.categoryId, getSubcategory(), false);
        }
    }
}