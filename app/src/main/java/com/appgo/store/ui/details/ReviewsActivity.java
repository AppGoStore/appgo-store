package com.appgo.store.ui.details;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.appgo.store.Constants;
import com.appgo.store.R;
import com.appgo.store.model.items.ReviewItem;
import com.appgo.store.ui.single.activity.BaseActivity;
import com.appgo.store.viewmodel.ReviewsModel;
import com.dragons.aurora.playstoreapiv2.GooglePlayAPI;
import com.google.android.material.chip.ChipGroup;
import com.mikepenz.fastadapter.FastAdapter;
import com.mikepenz.fastadapter.adapters.ItemAdapter;
import com.mikepenz.fastadapter.scroll.EndlessRecyclerOnScrollListener;
import com.mikepenz.fastadapter.ui.items.ProgressItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ReviewsActivity extends BaseActivity {
    @BindView(R.id.reviews_recycler)
    RecyclerView recyclerView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.chip_group)
    ChipGroup chipGroup;

    private String packageName;
    private ReviewsModel reviewsModel;

    private FastAdapter fastAdapter;
    private ItemAdapter<ReviewItem> itemAdapter;
    private ItemAdapter<ProgressItem> progressItemAdapter;
    private EndlessRecyclerOnScrollListener endlessScrollListener;

    private GooglePlayAPI.REVIEW_SORT reviewSort = GooglePlayAPI.REVIEW_SORT.HIGHRATING;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reviews);
        ButterKnife.bind(this);
        setupActionBar();
        setupRecycler();

        Intent intent = getIntent();
        if (intent != null) {
            packageName = intent.getStringExtra(Constants.INTENT_PACKAGE_NAME);
            if (packageName != null && !packageName.isEmpty()) {
                reviewsModel = new ViewModelProvider(this).get(ReviewsModel.class);
                reviewsModel.getReviews().observe(this, this::dispatchToAdapter);
                reviewsModel.fetchReviews(packageName, reviewSort, true);
            }
        }

        chipGroup.setOnCheckedChangeListener((group, checkedId) -> {
            switch (checkedId) {
                case R.id.chip_high:
                    reviewSort = GooglePlayAPI.REVIEW_SORT.HIGHRATING;
                    break;
                case R.id.chip_helpful:
                    reviewSort = GooglePlayAPI.REVIEW_SORT.HELPFUL;
                    break;
                case R.id.chip_new:
                    reviewSort = GooglePlayAPI.REVIEW_SORT.NEWEST;
                    break;
            }
            purgeAdapterData();
            reviewsModel.fetchReviews(packageName, reviewSort, true);
        });
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(menuItem);
    }

    private void setupActionBar() {
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(true);
            actionBar.setTitle(getString(R.string.details_reviews));
        }
    }

    private void dispatchToAdapter(List<ReviewItem> reviewItems) {
        itemAdapter.add(reviewItems);
        recyclerView.post(() -> {
            progressItemAdapter.clear();
        });
    }

    private void purgeAdapterData() {
        recyclerView.post(() -> {
            progressItemAdapter.clear();
            itemAdapter.clear();
            endlessScrollListener.resetPageCount();
        });
    }

    private void setupRecycler() {
        fastAdapter = new FastAdapter();
        itemAdapter = new ItemAdapter<>();
        progressItemAdapter = new ItemAdapter<>();

        fastAdapter.addAdapter(0, itemAdapter);
        fastAdapter.addAdapter(1, progressItemAdapter);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        endlessScrollListener = new EndlessRecyclerOnScrollListener(progressItemAdapter) {
            @Override
            public void onLoadMore(int currentPage) {
                recyclerView.post(() -> {
                    progressItemAdapter.clear();
                    progressItemAdapter.add(new ProgressItem());
                });
                reviewsModel.fetchReviews(packageName, reviewSort, false);
            }
        };

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addOnScrollListener(endlessScrollListener);
        recyclerView.setAdapter(fastAdapter);
    }
}
