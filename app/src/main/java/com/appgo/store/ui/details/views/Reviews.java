package com.appgo.store.ui.details.views;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.appgo.store.AuroraApplication;
import com.appgo.store.R;
import com.appgo.store.model.App;
import com.appgo.store.model.Review;
import com.appgo.store.sheet.UserReviewBottomSheet;
import com.appgo.store.task.BaseTask;
import com.appgo.store.ui.details.DetailsActivity;
import com.appgo.store.ui.details.ReviewsActivity;
import com.appgo.store.ui.view.RatingView;
import com.appgo.store.util.Accountant;
import com.appgo.store.util.Log;
import com.appgo.store.util.ViewUtil;
import com.dragons.aurora.playstoreapiv2.GooglePlayAPI;
import com.google.android.material.chip.Chip;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class Reviews extends AbstractDetails {

    @BindView(R.id.root_layout)
    LinearLayout rootLayout;
    @BindView(R.id.average_rating)
    TextView txtAverageRating;
    @BindView(R.id.count_stars)
    TextView txtCountStars;
    @BindView(R.id.average_rating_star)
    RatingBar averageRatingBar;
    @BindView(R.id.avg_rating_layout)
    LinearLayout avgRatingLayout;
    @BindView(R.id.user_stars)
    RatingBar userRatingBar;
    @BindView(R.id.user_comment_layout)
    LinearLayout userCommentLayout;
    @BindView(R.id.review_delete)
    Chip chipDelete;

    private CompositeDisposable disposable = new CompositeDisposable();

    public Reviews(DetailsActivity activity, App app) {
        super(activity, app);
    }

    @Override
    public void draw() {
        ButterKnife.bind(this, activity);

        if (!app.isInPlayStore() || app.isEarlyAccess())
            return;

        averageRatingBar.setRating(app.getRating().getAverage());
        txtAverageRating.setText(String.format(Locale.getDefault(), "%.1f", app.getRating().getAverage()));

        int totalStars = 0;
        for (int starNum = 1; starNum <= 5; starNum++) {
            totalStars += app.getRating().getStars(starNum);
        }

        txtCountStars.setText(String.valueOf(totalStars));
        avgRatingLayout.removeAllViews();
        avgRatingLayout.addView(addAvgReviews(5, totalStars, app.getRating().getStars(5)));
        avgRatingLayout.addView(addAvgReviews(4, totalStars, app.getRating().getStars(4)));
        avgRatingLayout.addView(addAvgReviews(3, totalStars, app.getRating().getStars(3)));
        avgRatingLayout.addView(addAvgReviews(2, totalStars, app.getRating().getStars(2)));
        avgRatingLayout.addView(addAvgReviews(1, totalStars, app.getRating().getStars(1)));

        show(R.id.reviews_container);
        if (isReviewable(app))
            show(R.id.user_review_layout);

        Review review = app.getUserReview();
        if (null != review) {
            fillUserReview(review);
        }

        initUserReviewControls(app);
    }

    private RelativeLayout addAvgReviews(int number, int max, int rating) {
        return new RatingView(activity, number, max, rating);
    }

    private boolean isReviewable(App app) {
        return !Accountant.isAnonymous(context) && app.isInstalled() && !app.isTestingProgramOptedIn();
    }

    private void fillUserReview(Review review) {
        clearUserReview();
        app.setUserReview(review);
        userRatingBar.setRating(review.getRating());
        setTextOrHide(R.id.user_comment, review.getComment());
        setTextOrHide(R.id.user_title, review.getTitle());
        setText(R.id.rate, R.string.details_you_rated_this_app);
        chipDelete.setVisibility(View.VISIBLE);
        userCommentLayout.setVisibility(View.VISIBLE);
    }

    private void clearUserReview() {
        userRatingBar.setRating(0);
        setText(R.id.user_title, "");
        setText(R.id.user_comment, "");
        setText(R.id.rate, R.string.details_rate_this_app);
        chipDelete.setVisibility(View.GONE);
        userCommentLayout.setVisibility(View.GONE);
    }

    private Review getUpdatedUserReview(Review oldReview, int stars) {
        Review review = new Review();
        review.setRating(stars);
        if (null != oldReview) {
            review.setComment(oldReview.getComment());
            review.setTitle(oldReview.getTitle());
        }
        return review;
    }

    private void initUserReviewControls(final App app) {
        userRatingBar.setOnRatingBarChangeListener((ratingBar, rating, fromUser) -> {
            if (!fromUser) {
                return;
            }
            UserReviewBottomSheet reviewsBottomSheet = new UserReviewBottomSheet();
            reviewsBottomSheet.setApp(app);
            reviewsBottomSheet.setRating((int) rating);
            reviewsBottomSheet.show(activity.getSupportFragmentManager(), "USER_REVIEWS");
        });

        chipDelete.setOnClickListener(v -> {
            disposable.add(Observable.fromCallable(() -> new ReviewRemover(context)
                    .delete(app.getPackageName()))
                    .subscribeOn(Schedulers.computation())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe((success) -> {
                        if (success) {
                            clearUserReview();
                        } else {
                            Log.e("Error deleting the review");
                        }
                    }, err -> Log.e(err.getMessage())));
        });
    }

    private void setTextOrHide(int viewId, String text) {
        TextView textView = activity.findViewById(viewId);
        if (!TextUtils.isEmpty(text)) {
            textView.setText(text);
            textView.setVisibility(View.VISIBLE);
        } else {
            textView.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.more_reviews_layout)
    public void openReviewsActivity() {
        Intent intent = new Intent(activity, ReviewsActivity.class);
        intent.putExtra("INTENT_PACKAGE_NAME", app.getPackageName());
        activity.startActivity(intent, ViewUtil.getEmptyActivityBundle(activity));
    }

    static class ReviewRemover extends BaseTask {

        ReviewRemover(Context context) {
            super(context);
        }

        private boolean delete(String packageName) {
            try {
                GooglePlayAPI api = AuroraApplication.api;
                api.deleteReview(packageName);
                return true;
            } catch (Exception e) {
                Log.e(e.getMessage());
                return false;
            }
        }
    }
}