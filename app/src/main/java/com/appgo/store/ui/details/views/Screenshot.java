package com.appgo.store.ui.details.views;

import android.content.Intent;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.appgo.store.Constants;
import com.appgo.store.R;
import com.appgo.store.model.App;
import com.appgo.store.model.items.ScreenshotItem;
import com.appgo.store.ui.details.DetailsActivity;
import com.appgo.store.ui.single.activity.FullscreenImageActivity;
import com.mikepenz.fastadapter.adapters.FastItemAdapter;

import butterknife.BindView;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class Screenshot extends AbstractDetails {

    @BindView(R.id.recycler)
    RecyclerView recyclerView;

    public Screenshot(DetailsActivity activity, App app) {
        super(activity, app);
    }

    @Override
    public void draw() {
        if (app.getScreenshotUrls().size() > 0) {
            drawGallery();
        }
    }

    private void drawGallery() {
        FastItemAdapter<ScreenshotItem> fastItemAdapter = new FastItemAdapter<>();

        Observable.fromIterable(app.getScreenshotUrls())
                .subscribeOn(Schedulers.io())
                .map(ScreenshotItem::new)
                .toList()
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSuccess(fastItemAdapter::add)
                .subscribe();

        fastItemAdapter.setOnClickListener((view, screenshotItemIAdapter, screenshotItem, position) -> {
            Intent intent = new Intent(context, FullscreenImageActivity.class);
            intent.putExtra(Constants.INTENT_PACKAGE_NAME, app.getPackageName());
            intent.putExtra(Constants.STRING_EXTRA, gson.toJson(app));
            intent.putExtra(FullscreenImageActivity.INTENT_SCREENSHOT_NUMBER, position);
            context.startActivity(intent);
            return false;
        });

        recyclerView.setAdapter(fastItemAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
    }
}