package com.appgo.store.ui.intro;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.appgo.store.Constants;
import com.appgo.store.R;
import com.appgo.store.ui.preference.SettingsActivity;
import com.appgo.store.ui.single.activity.BaseActivity;
import com.appgo.store.ui.single.activity.GoogleLoginActivity;
import com.appgo.store.ui.single.activity.SplashActivity;
import com.appgo.store.util.Accountant;
import com.appgo.store.util.ApiBuilderUtil;
import com.appgo.store.util.ContextUtil;
import com.appgo.store.util.NetworkUtil;
import com.appgo.store.util.PrefUtil;
import com.appgo.store.util.ViewUtil;
import com.appgo.store.util.diff.NavigationUtil;
import com.google.android.material.button.MaterialButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class IntroActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    NavController navController;
    @BindView(R.id.btn_next)
    MaterialButton btnNext;
    @BindView(R.id.btn_anonymous)
    MaterialButton btnAnonymous;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        ButterKnife.bind(this);
        setupActionbar();
        setupNavigation();

        //Do not show this again. Unless asked.
        PrefUtil.putBoolean(this, Constants.PREFERENCE_DO_NOT_SHOW_INTRO, true);
        Accountant.completeCheckout(this);
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_intro, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem menuItem) {
        if (menuItem.getItemId() == R.id.action_setting) {
            startActivity(new Intent(this, SettingsActivity.class), ViewUtil.getEmptyActivityBundle(this));
            return true;
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Accountant.isLoggedIn(this)) {
            NavigationUtil.launchAuroraActivity(this);
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        compositeDisposable.dispose();
        super.onDestroy();
    }

    private void setupActionbar() {
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setElevation(0f);
        }
    }

    private void setupNavigation() {
        navController = Navigation.findNavController(this, R.id.nav_host_intro);
        navController.addOnDestinationChangedListener((controller, destination, arguments) -> {
            switch (destination.getId()) {
                case R.id.welcomeFragment: {
                    btnNext.setOnClickListener(v -> moveForward(2));
                    break;
                }
                case R.id.permFragment: {
                    btnNext.setOnClickListener(askPermListener());
                    if (isPermissionGranted())
                        loginAnonymous();
                    break;
                }
                case R.id.loginFragment: {
                    btnNext.setText(R.string.action_logging_in);
                    btnNext.setOnClickListener(v -> {
                        loginAnonymous();
                    });
                }

            }
        });
    }

    private void moveForward(int pos) {
        switch (pos) {
            case 1:
                navController.navigate(R.id.welcomeFragment);
                break;
            case 2:
                navController.navigate(R.id.permFragment);
                break;
            case 3:
                navController.navigate(R.id.loginFragment);
                break;
            case 4:
                finishAfterTransition();
                break;
        }
    }

    public void loginAnonymous() {
        if (!NetworkUtil.isConnected(this)) {
            Toast.makeText(this, getString(R.string.error_no_network), Toast.LENGTH_SHORT).show();
            if (navController.getCurrentDestination() != null &&
                    navController.getCurrentDestination().getId() != R.id.loginFragment) {
                moveForward(3);
            }
            return;
        }

        Disposable disposable = Observable.fromCallable(() -> ApiBuilderUtil
                .login(this))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(d -> {
                    btnNext.setText(getText(R.string.action_logging_in));
                    btnNext.setEnabled(false);
                    progressBar.setVisibility(View.VISIBLE);
                })
                .subscribe(api -> {
                    if (api != null) {
                        Toast.makeText(this, getText(R.string.toast_login_success), Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(this, SplashActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        startActivity(intent, ViewUtil.getEmptyActivityBundle(this));
                        supportFinishAfterTransition();
                    } else {
                        Toast.makeText(this, getText(R.string.toast_login_failed), Toast.LENGTH_LONG).show();
                        ContextUtil.runOnUiThread(this::resetAnonymousLogin);
                    }
                }, err -> {
                    Toast.makeText(this, getText(R.string.toast_login_failed), Toast.LENGTH_LONG).show();
                    ContextUtil.runOnUiThread(this::resetAnonymousLogin);
                });
        compositeDisposable.add(disposable);
    }

    public void loginGoogle() {
        if (!NetworkUtil.isConnected(this)) {
            Toast.makeText(this, getString(R.string.error_no_network), Toast.LENGTH_SHORT).show();
            return;
        }
        startActivity(new Intent(this, GoogleLoginActivity.class), ViewUtil.getEmptyActivityBundle(this));
    }

    private void resetAnonymousLogin() {
        btnNext.setEnabled(true);
        btnNext.setText(getText(R.string.action_logging_in));
        progressBar.setVisibility(View.INVISIBLE);
    }

    private View.OnClickListener askPermListener() {
        btnNext.setText(R.string.action_ask);
        return v -> {
            checkPermissions();
        };
    }

    private boolean isPermissionGranted() {
        return ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    private void checkPermissions() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                1337);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1337: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    loginAnonymous();
                } else {
                    Toast.makeText(this, "Denied", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
}
