package com.appgo.store.ui.main.fragment.home;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.appgo.store.AuroraApplication;
import com.appgo.store.Constants;
import com.appgo.store.enums.ErrorType;
import com.appgo.store.model.App;
import com.appgo.store.model.items.ClusterItem;
import com.appgo.store.task.FeaturedAppsTask;
import com.appgo.store.util.PrefUtil;
import com.appgo.store.util.Util;
import com.appgo.store.viewmodel.BaseViewModel;
import com.dragons.aurora.playstoreapiv2.GooglePlayAPI;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Calendar;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class HomeAppsModel extends BaseViewModel {

    private Application application;
    private CompositeDisposable disposable = new CompositeDisposable();

    private Gson gson = new GsonBuilder().excludeFieldsWithModifiers(Modifier.TRANSIENT).create();

    private MutableLiveData<List<ClusterItem>> mutableTopGames = new MutableLiveData<>();
    private MutableLiveData<List<ClusterItem>> mutableTopApps = new MutableLiveData<>();
    private MutableLiveData<List<ClusterItem>> mutableTopAppGallery = new MutableLiveData<>();
    private MutableLiveData<ErrorType> mutableError = new MutableLiveData<>();

    public HomeAppsModel(@NonNull Application application) {
        super(application);
        this.application = application;
        this.api = AuroraApplication.api;

        //Fetch top chart apps
        fetchAppsFromCache(Constants.TOP_APPS);
        fetchAppsFromCache(Constants.TOP_GAME);
        fetchAppsFromCache(Constants.TOP_APP_GALLERY);
    }

    public LiveData<ErrorType> getError() {
        return mutableError;
    }

    public LiveData<List<ClusterItem>> getTopGames() {
        return mutableTopGames;
    }

    public LiveData<List<ClusterItem>> getTopApps() {
        return mutableTopApps;
    }

    public LiveData<List<ClusterItem>> getTopAppGallery() {
        return mutableTopAppGallery;
    }

    private void fetchAppsFromCache(String categoryId) {
        Type type = new TypeToken<List<App>>() {
        }.getType();
        String jsonString = PrefUtil.getString(application, categoryId);
        List<App> appList = gson.fromJson(jsonString, type);
        if (appList != null && !appList.isEmpty()) {
            Observable
                    .fromIterable(appList)
                    .map(ClusterItem::new)
                    .toList()
                    .doOnSuccess(clusterItems -> {
                        switch (categoryId) {
                            case Constants.TOP_APPS:
                                mutableTopApps.setValue(clusterItems);
                                break;
                            case Constants.TOP_GAME:
                                mutableTopGames.setValue(clusterItems);
                                break;
                            case Constants.TOP_APP_GALLERY:
                                mutableTopAppGallery.setValue(clusterItems);
                                break;
                        }
                    })
                    .doOnError(this::handleError)
                    .subscribe();

        } else
            fetchApps(categoryId);
    }

    private void fetchApps(String categoryId) {
        Observable.fromCallable(() -> new FeaturedAppsTask(application)
                .getApps(api, getPlayCategoryId(categoryId), GooglePlayAPI.SUBCATEGORY.TOP_FREE))
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(apps -> {
                    saveToCache(apps, categoryId);
                    Util.setCacheCreateTime(application, Calendar.getInstance().getTimeInMillis());
                    return Observable.fromIterable(apps).map(ClusterItem::new);
                })
                .toList()
                .doOnSuccess(clusterItems -> {
                    switch (categoryId) {
                        case Constants.TOP_APPS:
                            mutableTopApps.setValue(clusterItems);
                            break;
                        case Constants.TOP_GAME:
                            mutableTopGames.setValue(clusterItems);
                            break;
                        case Constants.TOP_APP_GALLERY:
                            mutableTopAppGallery.setValue(clusterItems);
                            break;
                    }
                })
                .doOnError(this::handleError)
                .subscribe();
    }

    private String getPlayCategoryId(String categoryId) {
        switch (categoryId) {
            case Constants.TOP_GAME:
                return Constants.CATEGORY_GAME;
            case Constants.TOP_APP_GALLERY:
                return Constants.CATEGORY_APP_GALLERY;
            default:
                return Constants.CATEGORY_APPS;
        }
    }

    private void saveToCache(List<App> appList, String key) {
        String jsonString = gson.toJson(appList);
        PrefUtil.putString(application, key, jsonString);
    }

    @Override
    protected void onCleared() {
        disposable.dispose();
        super.onCleared();
    }
}
