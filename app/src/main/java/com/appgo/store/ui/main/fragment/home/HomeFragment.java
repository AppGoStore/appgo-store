package com.appgo.store.ui.main.fragment.home;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.appgo.store.Constants;
import com.appgo.store.R;
import com.appgo.store.SharedPreferencesTranslator;
import com.appgo.store.model.App;
import com.appgo.store.model.items.ClusterItem;
import com.appgo.store.ui.category.CategoryAppsActivity;
import com.appgo.store.ui.details.DetailsActivity;
import com.appgo.store.ui.main.AuroraActivity;
import com.appgo.store.ui.single.fragment.BaseFragment;
import com.appgo.store.util.Util;
import com.appgo.store.util.ViewUtil;
import com.google.android.material.button.MaterialButton;
import com.mikepenz.fastadapter.adapters.FastItemAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeFragment extends BaseFragment {

    @BindView(R.id.recycler_top_apps)
    RecyclerView recyclerTopApps;
    @BindView(R.id.recycler_top_games)
    RecyclerView recyclerTopGames;
    @BindView(R.id.recycler_app_gallery)
    RecyclerView recyclerAppGallery;

    @BindView(R.id.btn_top_apps)
    MaterialButton btnTopApps;
    @BindView(R.id.btn_top_games)
    MaterialButton btnTopGames;
    @BindView(R.id.btn_top_app_gallery)
    MaterialButton btnTopAppGallery;

    private SharedPreferencesTranslator translator;

    private HomeAppsModel model;
    private FastItemAdapter<ClusterItem> topAppItemAdapter;
    private FastItemAdapter<ClusterItem> topGameItemAdapter;
    private FastItemAdapter<ClusterItem> topAppGalleryItemAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        translator = new SharedPreferencesTranslator(Util.getPrefs(requireContext()));
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setupRecyclers();

        model = new ViewModelProvider(this).get(HomeAppsModel.class);
        model.getTopApps().observe(getViewLifecycleOwner(), clusterItems -> topAppItemAdapter.add(clusterItems));
        model.getTopGames().observe(getViewLifecycleOwner(), clusterItems -> topGameItemAdapter.add(clusterItems));
        model.getTopAppGallery().observe(getViewLifecycleOwner(), clusterItems -> topAppGalleryItemAdapter.add(clusterItems));
        model.getError().observe(getViewLifecycleOwner(), errorType -> {

        });
    }

    @Override
    public void onResume() {
        super.onResume();
        topAppItemAdapter.notifyAdapterDataSetChanged();
        topGameItemAdapter.notifyAdapterDataSetChanged();
        topAppGalleryItemAdapter.notifyAdapterDataSetChanged();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void init() {
        setupButtons();
    }

    private void setupButtons() {
        btnTopApps.setOnClickListener(v -> openCategoryAppsActivity(Constants.CATEGORY_APPS));
        btnTopGames.setOnClickListener(v -> openCategoryAppsActivity(Constants.CATEGORY_GAME));
        btnTopAppGallery.setOnClickListener(v -> openCategoryAppsActivity(Constants.CATEGORY_APP_GALLERY));
    }

    private void setupRecyclers() {
        topAppItemAdapter = new FastItemAdapter<>();
        topGameItemAdapter = new FastItemAdapter<>();
        topAppGalleryItemAdapter = new FastItemAdapter<>();

        recyclerTopApps.setAdapter(topAppItemAdapter);
        recyclerTopApps.setLayoutManager(new GridLayoutManager(requireContext(), 2, RecyclerView.HORIZONTAL, false));

        recyclerTopGames.setAdapter(topGameItemAdapter);
        recyclerTopGames.setLayoutManager(new GridLayoutManager(requireContext(), 2, RecyclerView.HORIZONTAL, false));

        recyclerAppGallery.setAdapter(topAppGalleryItemAdapter);
        recyclerAppGallery.setLayoutManager(new GridLayoutManager(requireContext(), 2, RecyclerView.HORIZONTAL, false));

        topAppItemAdapter.setOnClickListener((view, clusterItemIAdapter, clusterItem, integer) -> {
            openDetailsActivity(clusterItem.getApp());
            return false;
        });
        topGameItemAdapter.setOnClickListener((view, clusterItemIAdapter, clusterItem, integer) -> {
            openDetailsActivity(clusterItem.getApp());
            return false;
        });
        topAppGalleryItemAdapter.setOnClickListener((view, clusterItemIAdapter, clusterItem, integer) -> {
            openDetailsActivity(clusterItem.getApp());
            return false;
        });
    }

    private void openDetailsActivity(App app) {
        final Intent intent = new Intent(requireContext(), DetailsActivity.class);
        intent.putExtra(Constants.INTENT_PACKAGE_NAME, app.getPackageName());
        intent.putExtra(Constants.STRING_EXTRA, gson.toJson(app));
        startActivity(intent, ViewUtil.getEmptyActivityBundle((AppCompatActivity) requireActivity()));
    }

    private void openCategoryAppsActivity(String categoryId) {
        Intent intent = new Intent(requireContext(), CategoryAppsActivity.class);
        intent.putExtra("CategoryId", categoryId);
        intent.putExtra("CategoryName", translator.getString(categoryId));
        requireContext().startActivity(intent, ViewUtil.getEmptyActivityBundle((AuroraActivity) requireContext()));
    }
}
