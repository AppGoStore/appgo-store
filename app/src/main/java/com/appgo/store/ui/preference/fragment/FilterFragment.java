package com.appgo.store.ui.preference.fragment;

import android.os.Bundle;

import androidx.preference.PreferenceFragmentCompat;

import com.appgo.store.Constants;
import com.appgo.store.R;

public class FilterFragment extends PreferenceFragmentCompat {

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        getPreferenceManager().setSharedPreferencesName(Constants.SHARED_PREFERENCES_KEY);
        setPreferencesFromResource(R.xml.preferences_filter, rootKey);
    }
}
