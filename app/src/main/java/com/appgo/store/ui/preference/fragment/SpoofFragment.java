package com.appgo.store.ui.preference.fragment;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.appgo.store.Constants;
import com.appgo.store.R;
import com.appgo.store.manager.SpoofManager;
import com.appgo.store.task.DeviceInfoBuilder;
import com.appgo.store.ui.single.fragment.BaseFragment;
import com.appgo.store.ui.spoof.GenericSpoofActivity;
import com.appgo.store.util.ContextUtil;
import com.appgo.store.util.Log;
import com.appgo.store.util.PrefUtil;
import com.appgo.store.util.ViewUtil;
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;

import org.apache.commons.lang3.StringUtils;

import java.util.Properties;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class SpoofFragment extends BaseFragment {

    @BindView(R.id.device_avatar)
    ImageView imgDeviceAvatar;
    @BindView(R.id.device_model)
    TextView txtDeviceModel;
    @BindView(R.id.device_info)
    TextView txtDeviceInfo;
    @BindView(R.id.export_fab)
    ExtendedFloatingActionButton exportFab;

    private String deviceName;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_spoof, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isSpoofed())
            drawSpoofedDevice();
        else
            drawDevice();
    }

    @OnClick(R.id.card_device)
    public void openDeviceSpoof() {
        Intent intent = new Intent(requireContext(), GenericSpoofActivity.class);
        intent.putExtra(Constants.FRAGMENT_NAME, Constants.FRAGMENT_SPOOF_DEVICE);
        startActivity(intent, ViewUtil.getEmptyActivityBundle((AppCompatActivity) requireActivity()));
    }

    @OnClick(R.id.card_locale)
    public void openLocaleSpoof() {
        Intent intent = new Intent(requireContext(), GenericSpoofActivity.class);
        intent.putExtra(Constants.FRAGMENT_NAME, Constants.FRAGMENT_SPOOF_LOCALE);
        startActivity(intent, ViewUtil.getEmptyActivityBundle((AppCompatActivity) requireActivity()));
    }

    @OnClick(R.id.card_geolocation)
    public void openGeoSpoof() {
        Intent intent = new Intent(requireContext(), GenericSpoofActivity.class);
        intent.putExtra(Constants.FRAGMENT_NAME, Constants.FRAGMENT_SPOOF_GEOLOCATION);
        startActivity(intent, ViewUtil.getEmptyActivityBundle((AppCompatActivity) requireActivity()));
    }

    @OnClick(R.id.export_fab)
    public void exportConfig() {
        Observable.fromCallable(() -> new DeviceInfoBuilder(requireContext())
                .build())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(success -> {
                    ContextUtil.toast(requireContext(), success
                            ? R.string.action_export_info
                            : R.string.action_export_info_failed);
                })
                .doOnError(throwable -> Log.e(throwable.getMessage()))
                .subscribe();
    }

    private boolean isSpoofed() {
        deviceName = PrefUtil.getString(requireContext(), Constants.PREFERENCE_SPOOF_DEVICE);
        return (deviceName.contains("device-"));
    }

    private void drawDevice() {
        txtDeviceModel.setText(new StringBuilder()
                .append(Build.MODEL)
                .append(" | ")
                .append(Build.DEVICE));

        txtDeviceInfo.setText(new StringBuilder()
                .append(Build.MANUFACTURER)
                .append(" | ")
                .append(Build.BOARD));
    }

    private void drawSpoofedDevice() {
        final Properties properties = new SpoofManager(this.requireContext()).getProperties(deviceName);

        txtDeviceModel.setText(StringUtils.joinWith(" | ",
                properties.getProperty("UserReadableName"),
                properties.getProperty(Constants.BUILD_DEVICE)));

        txtDeviceInfo.setText(StringUtils.joinWith(" | ",
                properties.getProperty(Constants.BUILD_MANUFACTURER),
                properties.getProperty(Constants.BUILD_HARDWARE)));
    }
}
