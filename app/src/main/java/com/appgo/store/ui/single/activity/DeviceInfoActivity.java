package com.appgo.store.ui.single.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.widget.LinearLayout;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;

import com.appgo.store.Constants;
import com.appgo.store.R;
import com.appgo.store.manager.SpoofManager;
import com.appgo.store.ui.accounts.AccountsActivity;
import com.appgo.store.ui.view.PropertyView;
import com.appgo.store.util.Accountant;
import com.appgo.store.util.ContextUtil;
import com.appgo.store.util.Log;
import com.appgo.store.util.PrefUtil;
import com.dragons.aurora.playstoreapiv2.PropertiesDeviceInfoProvider;
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DeviceInfoActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.incognito_fab)
    ExtendedFloatingActionButton incognito_fab;
    @BindView(R.id.device_info)
    LinearLayout root;

    private String deviceName;
    private int deviceIndex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_info);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
        }

        onNewIntent(getIntent());
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        deviceName = intent.getStringExtra(Constants.INTENT_DEVICE_NAME);
        deviceIndex = intent.getIntExtra(Constants.INTENT_DEVICE_INDEX, 0);
        if (TextUtils.isEmpty(deviceName)) {
            Log.d("No device name given");
            finish();
            return;
        }

        Properties properties = new SpoofManager(this).getProperties(deviceName);
        toolbar.setTitle(properties.getProperty("UserReadableName"));
        List<String> keys = new ArrayList<>();
        for (Object key : properties.keySet()) {
            keys.add((String) key);
        }

        Collections.sort(keys);
        for (String key : keys) {
            addCards(root, key, ((String) properties.get(key)).replace(",", ", "));
        }

        setupButtons();
    }

    private void addCards(LinearLayout root, String key, String value) {
        root.addView(new PropertyView(this, key, value));
    }

    private void setupButtons() {
        incognito_fab = findViewById(R.id.incognito_fab);
        incognito_fab.show();
        incognito_fab.setOnClickListener(click -> showConfirmationDialog());
    }

    private boolean isDeviceDefinitionValid(String deviceName) {
        final PropertiesDeviceInfoProvider deviceInfoProvider = new PropertiesDeviceInfoProvider();
        deviceInfoProvider.setProperties(new SpoofManager(this).getProperties(deviceName));
        deviceInfoProvider.setLocaleString(Locale.getDefault().toString());
        return deviceInfoProvider.isValid();
    }

    private void showConfirmationDialog() {
        new AlertDialog.Builder(this)
                .setMessage(R.string.pref_device_to_pretend_to_be_toast)
                .setTitle(R.string.dialog_title_logout)
                .setPositiveButton(R.string.action_logout, (dialogInterface, i) -> {
                    if (!TextUtils.isEmpty(deviceName) && !isDeviceDefinitionValid(deviceName)) {
                        ContextUtil.toast(this, R.string.error_invalid_device_definition);
                    } else {
                        PrefUtil.putString(this, Constants.PREFERENCE_SPOOF_DEVICE, deviceName);
                        //PrefUtil.putInteger(this, Constants.PREFERENCE_DEVICE_TO_PRETEND_TO_BE_INDEX, deviceIndex);
                    }
                    Accountant.completeCheckout(this);
                    dialogInterface.dismiss();
                    startActivity(new Intent(this, AccountsActivity.class));
                    this.finish();
                })
                .setNegativeButton(android.R.string.cancel, null)
                .show();
    }
}
