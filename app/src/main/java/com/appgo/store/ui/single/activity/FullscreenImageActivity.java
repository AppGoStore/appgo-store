package com.appgo.store.ui.single.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import com.appgo.store.Constants;
import com.appgo.store.R;
import com.appgo.store.adapter.BigScreenshotsAdapter;
import com.appgo.store.model.App;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FullscreenImageActivity extends BaseActivity {

    static public final String INTENT_SCREENSHOT_NUMBER = "INTENT_SCREENSHOT_NUMBER";

    @BindView(R.id.gallery)
    RecyclerView recyclerView;

    private App app;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN
        );
        setContentView(R.layout.activity_fullscreen_screenshots);
        ButterKnife.bind(this);
        onNewIntent(getIntent());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent != null) {

            stringExtra = intent.getStringExtra(Constants.STRING_EXTRA);
            intExtra = intent.getIntExtra(INTENT_SCREENSHOT_NUMBER, 0);

            if (stringExtra != null) {
                app = gson.fromJson(stringExtra, App.class);
                setupRecycler();
            }
        } else {
            finishAfterTransition();
        }
    }

    private void setupRecycler() {
        final SnapHelper snapHelper = new PagerSnapHelper();
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false);
        snapHelper.attachToRecyclerView(recyclerView);

        recyclerView.setAdapter(new BigScreenshotsAdapter(app.getScreenshotUrls(), this));
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.scrollToPosition(intExtra);
    }
}