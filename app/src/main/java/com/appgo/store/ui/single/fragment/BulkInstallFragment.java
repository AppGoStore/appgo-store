package com.appgo.store.ui.single.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.appgo.store.Constants;
import com.appgo.store.R;
import com.appgo.store.events.Event;
import com.appgo.store.model.App;
import com.appgo.store.model.items.BulkItem;
import com.appgo.store.ui.details.DetailsActivity;
import com.appgo.store.ui.view.ViewFlipper2;
import com.appgo.store.util.PackageUtil;
import com.appgo.store.util.ViewUtil;
import com.appgo.store.viewmodel.BulkInstallViewModel;
import com.google.android.material.button.MaterialButton;
import com.mikepenz.fastadapter.adapters.FastItemAdapter;
import com.mikepenz.fastadapter.select.SelectExtension;

import java.util.HashSet;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BulkInstallFragment extends BaseFragment {

    @BindView(R.id.viewFlipper)
    ViewFlipper2 viewFlipper;
    @BindView(R.id.swipeLayout)
    SwipeRefreshLayout swipeLayout;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.btnInstall)
    MaterialButton btnInstall;

    private FastItemAdapter<BulkItem> fastItemAdapter;
    private SelectExtension<BulkItem> selectExtension;

    private Set<App> selectedAppSet = new HashSet<>();
    private BulkInstallViewModel model;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_bulk_install, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setupRecycler();
        model = new ViewModelProvider(this).get(BulkInstallViewModel.class);
        model.getApps().observe(getViewLifecycleOwner(), items -> {
            swipeLayout.setRefreshing(false);
            fastItemAdapter.clear();
            fastItemAdapter.add(items);
            updateData();
        });
        model.getEvent().observe(getViewLifecycleOwner(), this::handleInstallEvent);
        model.installAppError.observe(getViewLifecycleOwner(), this::showAppNotInstalledAlert);
        swipeLayout.setRefreshing(true);
        swipeLayout.setOnRefreshListener(() -> swipeLayout.setRefreshing(false));
        btnInstall.setOnClickListener(bulkInstallListener());
    }

    private View.OnClickListener bulkInstallListener() {
        return v -> {
            btnInstall.setText(getString(R.string.details_installing));
            btnInstall.setEnabled(false);
            model.installApps(selectedAppSet);
        };
    }

    private void updateData() {
        updateButtons();
        if (fastItemAdapter != null && fastItemAdapter.getAdapterItems().size() > 0) {
            viewFlipper.switchState(ViewFlipper2.DATA);
        } else {
            viewFlipper.switchState(ViewFlipper2.EMPTY);
        }
    }

    private void setupRecycler() {
        fastItemAdapter = new FastItemAdapter<>();
        selectExtension = new SelectExtension<>(fastItemAdapter);

        fastItemAdapter.setOnPreClickListener((view, bulkItemIAdapter, item, position) -> true);
        fastItemAdapter.setOnClickListener((view, bulkItemIAdapter, item, position) -> {
            final App app = item.getApp();
            final Intent intent = new Intent(requireContext(), DetailsActivity.class);
            intent.putExtra(Constants.INTENT_PACKAGE_NAME, app.getPackageName());
            intent.putExtra(Constants.STRING_EXTRA, gson.toJson(app));
            startActivity(intent, ViewUtil.getEmptyActivityBundle((AppCompatActivity) requireActivity()));
            return false;
        });

        fastItemAdapter.addExtension(selectExtension);
        fastItemAdapter.addEventHook(new BulkItem.CheckBoxClickEvent());

        selectExtension.setMultiSelect(true);
        selectExtension.setSelectionListener((item, selected) -> {
            System.out.println("selectExtension item " + item.getApp().getDisplayName() + " selected " + selected + " isChecked " + item.isChecked());
            if (selected) {
                if (!PackageUtil.isInstalled(requireContext(), item.getApp()))
                    selectedAppSet.add(item.getApp());
            } else
                selectedAppSet.remove(item.getApp());
            updateData();
        });
        recyclerView.setAdapter(fastItemAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));
    }

    private void updateButtons() {
        int size = selectExtension.getSelectedItems().size();
        btnInstall.setEnabled(size > 0);
    }

    private void handleInstallEvent(Event event) {
        Event.SubType subType = event.getSubType();
        if (subType.equals(Event.SubType.INSTALLED) && event.getStringExtra() != null) {
            for (BulkItem bulkItem : fastItemAdapter.getAdapterItems()) {
                if (bulkItem.getPackageName().equals(event.getStringExtra())) {
                    if (event.getStatus() == 0) {
                        String message = getString(R.string.installer_status_failure) + " " + bulkItem.getApp().getDisplayName();
                        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                    }
                    if (event.getStatus() == 1) {
                        selectedAppSet.remove(bulkItem.getApp());
                        bulkItem.setChecked(false);
                    }
                    int position = fastItemAdapter.getAdapterPosition(bulkItem);
                    fastItemAdapter.notifyAdapterItemChanged(position);
                    break;
                }
            }
        }
    }

    private void showAppNotInstalledAlert(String appName) {
        new AlertDialog.Builder(requireContext())
                .setMessage(getString(R.string.app_not_available, appName))
                .setPositiveButton(android.R.string.ok, (dialogInterface, i) -> {
                    removeAppFromSelectedSet(appName);
                    dialogInterface.dismiss();
                }).show();
    }

    private void removeAppFromSelectedSet(String appName) {
        for (BulkItem bulkItem : fastItemAdapter.getAdapterItems()) {
            if (bulkItem.getApp().getDisplayName().equals(appName)) {
                selectedAppSet.remove(bulkItem.getApp());

                bulkItem.setChecked(false);
                bulkItem.setSelected(false);
                int position = fastItemAdapter.getAdapterPosition(bulkItem);
                fastItemAdapter.notifyAdapterItemChanged(position);
                updateButtons();
                break;
            }
        }
    }
}
