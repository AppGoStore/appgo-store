package com.appgo.store.ui.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ViewFlipper;

public class ViewFlipper2 extends ViewFlipper {

    public static final int PROGRESS = 0;
    public static final int DATA = 1;
    public static final int EMPTY = 2;
    public static final int ERROR = 3;
    public static final int NO_NETWORK = 4;

    public ViewFlipper2(Context context) {
        super(context);
        setDisplayedChild(PROGRESS);
    }

    public ViewFlipper2(Context context, AttributeSet attrs) {
        super(context, attrs);
        setDisplayedChild(PROGRESS);
    }

    public void switchState(int state) {
        setDisplayedChild(state);
    }

}
