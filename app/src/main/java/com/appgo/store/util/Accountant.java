package com.appgo.store.util;

import android.content.Context;

import com.appgo.store.model.LoginInfo;

public class Accountant {
    public static final String DATA = "DATA";
    public static final String EMAIL = "EMAIL";
    public static final String PROFILE_NAME = "PROFILE_NAME";
    public static final String PROFILE_AVATAR = "PROFILE_AVATAR";
    public static final String PROFILE_BACKGROUND = "PROFILE_BACKGROUND";
    public static final String LOGGED_IN = "LOGGED_IN";
    public static final String ANONYMOUS = "ANONYMOUS";

    public static Boolean isLoggedIn(Context context) {
        return PrefUtil.getBoolean(context, LOGGED_IN);
    }

    public static Boolean isAnonymous(Context context) {
        return PrefUtil.getBoolean(context, ANONYMOUS);
    }

    public static String getUserName(Context context) {
        return PrefUtil.getString(context, PROFILE_NAME);
    }

    public static String getEmail(Context context) {

        return PrefUtil.getString(context, EMAIL);
    }

    public static String getImageURL(Context context) {
        return PrefUtil.getString(context, PROFILE_AVATAR);
    }

    public static String getBackgroundImageURL(Context context) {
        return PrefUtil.getString(context, PROFILE_BACKGROUND);
    }

    public static void completeCheckout(Context context) {
        PrefUtil.remove(context, LOGGED_IN);
        PrefUtil.remove(context, EMAIL);
        PrefUtil.remove(context, PROFILE_NAME);
        PrefUtil.remove(context, PROFILE_AVATAR);
        PrefUtil.remove(context, PROFILE_BACKGROUND);
        LoginInfo.removeSavedInstance(context);
    }

    public static void setLoggedIn(Context context) {
        PrefUtil.putBoolean(context, LOGGED_IN, true);
    }

    public static void setAnonymous(Context context, boolean value) {
        PrefUtil.putBoolean(context, ANONYMOUS, value);
    }
}
