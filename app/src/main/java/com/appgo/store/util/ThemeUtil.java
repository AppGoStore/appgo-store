package com.appgo.store.util;

import android.content.Context;
import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;

import com.appgo.store.R;
import com.appgo.store.manager.LocaleManager;

public class ThemeUtil {

    private int currentTheme;

    public static int getSelectedTheme(AppCompatActivity activity) {
        String theme = Util.getTheme(activity);
        switch (theme) {
            case "light":
                return R.style.AppTheme;
            case "dark":
                return R.style.AppTheme_Dark;
            case "black":
                return R.style.AppTheme_Black;
            default:
                return R.style.AppTheme;
        }
    }

    public static boolean isLightTheme(Context context) {
        String theme = Util.getTheme(context);
        switch (theme) {
            case "dark":
            case "black":
                return false;
            default:
                return true;
        }
    }

    public void onCreate(AppCompatActivity activity) {
        new LocaleManager(activity).setLocale();
        currentTheme = getSelectedTheme(activity);
        activity.setTheme(currentTheme);
    }

    public void onResume(AppCompatActivity activity) {
        if (currentTheme != getSelectedTheme(activity)) {
            Intent intent = activity.getIntent();
            activity.finish();
            activity.startActivity(intent);
        }
    }
}
