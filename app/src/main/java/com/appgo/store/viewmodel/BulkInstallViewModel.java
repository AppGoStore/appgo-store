package com.appgo.store.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.appgo.store.AuroraApplication;
import com.appgo.store.events.Event;
import com.appgo.store.exception.NotPurchasedException;
import com.appgo.store.model.App;
import com.appgo.store.model.AppBuilder;
import com.appgo.store.model.AppModel;
import com.appgo.store.model.items.BulkItem;
import com.appgo.store.task.AppGalleryTask;
import com.appgo.store.task.LiveUpdate;
import com.appgo.store.task.ObservableDeliveryData;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class BulkInstallViewModel extends BaseViewModel {

    private MutableLiveData<List<BulkItem>> data = new MutableLiveData<>();
    private MutableLiveData<Event> event = new MutableLiveData<>();
    public MutableLiveData<String> installAppError = new MutableLiveData<>();

    public BulkInstallViewModel(@NonNull Application application) {
        super(application);
        fetchApps();
        subscribeToInstallEvents();
    }

    public LiveData<List<BulkItem>> getApps() {
        return data;
    }

    public LiveData<Event> getEvent() {
        return event;
    }

    public void fetchApps() {
        Single<List<AppModel>> oobeRequest = Single.fromCallable(() -> new AppGalleryTask().getOobeApps());
        Single<List<AppModel>> oobePlayRequest = Single.fromCallable(() -> new AppGalleryTask().getOobePlayApps());
        disposable.add(Single.zip(oobeRequest, oobePlayRequest, (oobeApps, oobePlayApps) -> {
                    List<BulkItem> result = new ArrayList<>();
                    for (AppModel app: oobeApps) {
                        result.add(new BulkItem(AppBuilder.build(app, true)));
                    }
                    for (AppModel app: oobePlayApps) {
                        result.add(new BulkItem(AppBuilder.build(app, false)));
                    }
                    return result;
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSuccess(items -> data.setValue(items))
                .doOnError(error -> {
                    data.setValue(new ArrayList<>());
                    handleError(error);
                })
                .subscribe());
    }

    public void installApps(Set<App> selectedAppSet) {
        Observable.fromIterable(selectedAppSet)
                .flatMap(app -> new ObservableDeliveryData(getApplication()).getFavoriteDeliveryData(app))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError(error -> {
                    if (error instanceof NotPurchasedException) {
                        installAppError.setValue(error.getMessage());
                    }
                })
                .doOnNext(bundle -> new LiveUpdate(getApplication()).enqueueUpdate(bundle.getApp(), bundle.getAndroidAppDeliveryData()))
                .doOnError(this::handleError)
                .subscribe();
    }

    private void subscribeToInstallEvents() {
        disposable.add(AuroraApplication
                .getRxBus()
                .getBus()
                .subscribe(event -> this.event.setValue(event))
        );
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        disposable.dispose();
    }
}
