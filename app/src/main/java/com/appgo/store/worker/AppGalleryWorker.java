package com.appgo.store.worker;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.appgo.store.AuroraApplication;
import com.appgo.store.db.AppDao;
import com.appgo.store.db.AppDatabase;
import com.appgo.store.db.CategoryDao;
import com.appgo.store.model.AppCategoryModel;
import com.appgo.store.model.AppModel;
import com.appgo.store.model.AppResponse;
import com.appgo.store.network.AppGalleryApi;
import com.appgo.store.util.Log;

import java.util.List;

public class AppGalleryWorker extends Worker {

    public static final String TAG = "APP_GALLERY_WORKER";

    public AppGalleryWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
        Log.i("AppGalleryWorker started");
    }

    @NonNull
    @Override
    public Result doWork() {
        try {
            AppGalleryApi appGalleryApi = new AppGalleryApi();
            AppResponse response = appGalleryApi.loadAppGalleryApps();
            if (response != null) {
                updateAppData(AuroraApplication.getDatabase(), response.getApps(), response.getCategories());
                return Result.success();
            } else {
                return Result.failure();
            }
        } catch (Exception e) {
            return Result.failure();
        }
    }

    //    @Transaction
    private void updateAppData(AppDatabase database, List<AppModel> applications, List<AppCategoryModel> categories) {
        CategoryDao categoryDao = database.categories();
        categoryDao.truncate();
        for (AppCategoryModel category : categories) {
            categoryDao.insert(category);
        }
        Log.i("AppGalleryWorker: updated %s categories", categories.size());
        AppDao appDao = database.applications();
        appDao.truncate();
        for (AppModel app : applications) {
            appDao.insert(app);
        }
        Log.i("AppGalleryWorker: updated %s applications", applications.size());
    }
}
